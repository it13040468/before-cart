import os
import sys
import tornado
import json
from tornado.options import  options
from tornado import ioloop, web
from tornado.options import define
import json, traceback
from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop

import tornado.web

import subprocess
import ast
import getResults
#import DataAccess
import datetime

#create graph coodinates
class GraphService(tornado.web.RequestHandler):

    def get(self):

       
        productId = 0
        productId = int(self.get_argument('pid','0')) #get arguments
        coodinates =[]
         # call data access method
        coodinates.append(getResults.getPopularityValues(productId))

        coodinates = json.dumps(coodinates) #convert to json
        print coodinates
        self.set_status(200)
        self.write(coodinates)





#setting request handlers 
class BeforeCartAPI(tornado.web.Application):
    def __init__(self):
        handlers = [
            #Service class
                (r"/chart?",GraphService)
                                          
            ]
        tornado.web.Application.__init__(self,handlers)


#main program
def main():

    app = BeforeCartAPI() #application
    app.listen(8080)
    IOLoop.instance().start()

if __name__ == '__main__':
        
        main()