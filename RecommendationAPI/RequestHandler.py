import os
import sys
import tornado
import pymongo
from tornado.options import  options
from tornado import ioloop, web
from tornado.options import define
from tornado.wsgi import WSGIContainer
from tornado.web import FallbackHandler
import json, traceback
from bson.objectid import ObjectId
#from indico.error import IndicoError, RouteNotFound, ServerError
#from indico.utils import LOGGER
from tornado import httpserver
from tornado import gen
from tornado.ioloop import IOLoop
import sqlite3 as sqlite
import tornado.web
import logging
import logging.handlers
import subprocess
import ast
import sys
import datetime




#import FRS modules

from DataAccess import FriendshipManagerFRS
from DataAccess import SuggestionManagerFRS
from DataAccess import ChatHistoryFRS
from DataAccess import ChatAnalyzerFRS
from DataAccess import UserManagerFRS 
from DataAccess import DBConf
from DataAccess import ChatManagerFRS


#import PRS modules
from Service.SuggestionHandlerPRS import SuggestionHandlerPRS
from Service.UpdateHandlerPRS import UpdateHandlerPRS

#import GMI modules
from DataAccess import GetPopularityDetailsGMI

#import materials of Flask service
#from flask import Flask, render_template
#from flask.ext.socketio import SocketIO, emit
#from datetime import datetime
#import os


#import sys
#import tornado
#import pymongo
#from tornado.options import  options
#from tornado import ioloop, web


#from Service import UpdateHandlerPRS
#from Service import SuggestionHandlerPRS 
#from tornado.options import define

define("port", default=8080, help="port", type=int)
define("mongodb_host",default='localhost:27017', help='Monogo Database Host', type=str)
define("mongodb_name",default='Research', help='Database Name', type=str)
#define("user", default="root", help="tornado_api database user")
#define("password", default="", help="tornado_api database password")
define("init_db",default=1, help='Initisalize database', type=int)
define("static_path", default=os.path.join(os.path.dirname(os.path.realpath(__file__)),'..','client'), help='static path', type=str)
#adding local directory to path
sys.path.append(os.path.dirname(os.path.realpath(__file__)))


'''
Connecting to the mongodb database
'''
mongo_client = pymongo.MongoClient(options.mongodb_host)
db = mongo_client[options.mongodb_name]

static_path = options.static_path





#recommends uses to make relationships
class SuggestNewFriendsFRS(tornado.web.RequestHandler):
    def get(self):
        
        userId = self.get_argument('userId','0')
        #define logging
                
        #Data Access
        sMgr = SuggestionManagerFRS()
        uMgr = UserManagerFRS()

        #get user email
        email = uMgr.getUserEmail(str(userId))

        if email != '' and int( userId) != 0: #validate request
            
           # print (" Invoked suggest_new_pals_frs in FriendshipHandlerFRS.py \n TIME :"+ str(datetime.datetime.today())+"\n Returned:\n")
            #get list of users
            list = []
            users = sMgr.suggestNewFriends(email)
            for user in users: 
                #filter required fields of user
                #print("User:"+str(user))
                    item ={}               
                    item['firstname']= user['firstName']
                    item['email']= user['email']
                    item['userId'] =user['userId']
                    item['exp'] = user['exp']
                    list.append(item) 
            #write results
           #print list 
            print 'suggested new friends [OK]\n'           
            self.write(json.dumps(list))
            self.set_status(200)

        else:
            print ("Invalid userID or couldn't refine email;\n userId:"+str(userId)+" email="+str(email))
            self.set_status(400)
            self.write('400') 

#suggest freinds for chat
class SuggestFriendsForChatFRS(tornado.web.RequestHandler):
    def get(self):
        
          
        userId = self.get_argument('userId','0') #userid
        catId  =self.get_argument('catId','0') #product category id

        #check userid and catid
        if int(userId) > 0 and 0<int(catId)<9:

            sMgr = SuggestionManagerFRS()
            uMgr =UserManagerFRS()

             #get user email
            email = uMgr.getUserEmail(str(userId))
            #check email
            if str(email) != '':

              #get suggestion list
              suggestedList= sMgr.refineChatList(userId,email,str(catId))
              #print ("response: 200- Results:\n"+str(suggestedList))
              self.set_status(200)
              self.write(json.dumps(suggestedList))
              return

            #invalid email
            else:
               print("Invalid Email ;\n userId:"+str(userId))
               self.set_status(404)
               self.write('404')                    
            
        else:
            print ("Invalid userID or catId ;\n userId:"+str(userId))
            self.set_status(406)
            self.write('406')


#save a concluded chat data in db
class saveNewChatFRS(tornado.web.RequestHandler):

    #post method
    def post(self):
        friends =''
        userId = ''
        chatData =''
        try:
        # fetch  get user,freinds,conversation data to dictionary
            data =json.loads(str(self.request.body))
            list ={}
            for slot in  data:
              
              list.update({str(slot):str(data[slot])})
           

            print list
            userId= list['userId']
            friends=list['friends']
            chatData= list['chat']
            
            friends = ast.literal_eval(friends)


        except Exception ,e:
            print e.message
            self.write_error(417)
        finally:
            #if data is not empty
            if userId !='' and friends !='' and chatData != '':

                #invoke data access
                uMgr  = UserManagerFRS()
                cMgr = ChatHistoryFRS()
                cAnl = ChatAnalyzerFRS()

                #get user email 
                email = uMgr.getUserEmail(userId)

                #if email is not empty
                if email != '':

                    # analyze chat
                    
                    analysisSts = cAnl.prepareChatData(chatData,email)
                    #save chat
                    if analysisSts ==True:
                        result=cMgr.saveChatHistoryFRS(email,friends,chatData)

                    # if chat saved successfully
                    if result==True and analysisSts ==True:
                        print ('chat saved successfully')
                        self.write('-1')
                    else:
                        print ('saving chat returned error.Chat saved with errors ')
                        self.write('-1')
                else:
                    print 'Could not find matching email address .Chat did not saved in server userId'+str(userId)+" on,"+str(datetime.datetime.today())
                    self.write('-1')
            else:
                print 'invalid data given for userId'+str(userId)+",on "+str(datetime.datetime.today())
                self.set_status(203)
                self.write('-1')
    
    #get method
    def get(self):
        print ('get method is not supported')
        self.set_status(501)



class CreateUserFRS(tornado.web.RequestHandler):
    #create new user
    def post(self):
        
        #declare user data  dictionary
        data ={}

        #dispatch request data into dictionary
        username = self.get_argument('username','')
        data['username'] = str(username)
      
        password = self.get_argument('password','')
        data['password'] = str(password)
        
        firstName = self.get_argument('firstName','')
        data['firstName'] = str(firstName)

        lastName = self.get_argument('lastName','')
        data['lastName'] = str(lastName)
      
        phone = str(self.get_argument('phone',''))
        data['phone'] = str(phone)
                                       
        email = self.get_argument('email','')
        data['email'] = str(email)

        address = str(self.get_argument('address',''))
        data['address'] = str(address)
       
        state = str(self.get_argument('state',''))
        data['state'] = state
      
        postal = str(self.get_argument('postal',''))
        data['postal'] = postal

        #invoke data access
        uMgr =UserManagerFRS()

        #validate data in dictionary
        isValid = uMgr.validateUserData(data)

        #if userdata is valid
        if isValid == True:
            
            #create new user node
            retVal = uMgr.createNewUserNode(data)

            #saved in success 0< return new user id
            if retVal >0 :
                self.write(str(retVal))
                self.set_status(201)

            # already an  user exists for given email
            elif retVal == -1:  
                self.set_status(409)
                self.write('-1')

            #exception in data access
            elif retVal == 0:
                self.set_status(417)
                self.write('0')
                
        else: #validation failed
            self.write('0')

#update network strength
class UpdateAllFriendhipsFRS(tornado.web.RequestHandler):
    
    def get(self):
        
        userId = int(self.get_argument('userId','0'))
        #if user is is valid
        if userId >0:
            uMgr =UserManagerFRS()
            sMgr =SuggestionManagerFRS()
           
            email = uMgr.getUserEmail(str(userId)) #get user-email
            
            sMgr.upgradeRelationshipStrength(email)# upgrade  relationship ofstrength of user


            self.set_status(200)
        else:
            print 'failed'
            self.set_status(404)

 
#update user data
class UpdateUserDetailsFRS(tornado.web.RequestHandler):

    def post(self):
        #declare user data  dictionary
        data ={}

        #dispatch request data into dictionary
        username = self.get_argument('userId','0')
        
        data['userId'] = str(username)
        if int(data['userId'])>0:

            #get request data
            username = self.get_argument('username','')
            data['username'] = str(username)
      
            password = self.get_argument('password','')
            data['password'] = str(password)
        
            firstName = self.get_argument('firstName','')
            data['firstName'] = str(firstName)

            lastName = self.get_argument('lastName','')
            data['lastName'] = str(lastName)
      
            phone = str(self.get_argument('phone',''))
            data['phone'] = str(phone)
                                       
            email = self.get_argument('email','')
            data['email'] = str(email)

            address = str(self.get_argument('address',''))
            data['address'] = str(address)
       
            state = str(self.get_argument('state',''))
            data['state'] = state
      
            postal = str(self.get_argument('postal',''))
            data['postal'] = postal

            #invoke data access
            uMgr =UserManagerFRS()
            #uMgr.validateUserData(data)
            
            

            #if userdata is valid
            if int(data['userId'] )> 0:
                #update properties
               for key in data:

                   if str(data[key]) !=  '' :

                        #if phone is invalid
                        if key =='phone' and len(data[key]) != 10:
                            print 'phone  not updated invalid length'
                            continue

                        #if postal code is invalid
                        if key =='postal' and len(data[key]) !=5:
                            print 'postal not updated length != 5'
                            continue

                        #update  property
                        sts=uMgr.updateProperty(data['userId'],key,data[key])

                        #if update is success
                        if sts == True:
                            print "KEY: "+str(key)+":VAL "+str(data[key])+" UPDATED"

                        else:
                            print "KEY: "+str(key)+"VAL: "+str(data[key])+" NOT UPDATED"

                   else: print str(key)+" IS SKIPPED"

               self.write('200')

               print "USER_ID: "+str(data['userId'])+' IS UPDATED'

               self.set_status(200)

            else: #validation failed
               self.write('417')
        else:
            self.set_status(404)
            self.write('404')          


#delete existing user node                 
class RemoveUserFRS(tornado.web.RequestHandler): 
    
    def delete(self):

        #get user id  
        userId  = self.get_argument('userId', '0')
        #check id validity
        if int(userId)>0:
            
            uMgr =  UserManagerFRS()
            fMgr = FriendshipManagerFRS()
            #get email of user
            email = uMgr.getUserEmail(userId)

            #check email validity
            if email !='':

                #remove all relationships
                relRemovalSts =fMgr.removeAllRelationships(email)
                #remove user node
                removalSts = uMgr.removeUserNode(email)

                # if node removed successfully
                if removalSts ==True and relRemovalSts==True :
                    self.write('200')
                    self.set_status(200)

                else:
                    self.write('417')
                    self.set_status(417)
            
            #no such email found
            else:
                self.write('404')
                self.set_status(404)


        # given user id invalid
        else:
            self.write('406')
            self.set_status(417)

        

             
        
class SendFriendRequestFRS(tornado.web.RequestHandler):

    # send friendship request to an user
     def get(self):
             
         uMgr = UserManagerFRS()
         fMgr = FriendshipManagerFRS()
         isMade =False #relationship status 

         #filter  user ids from client request
         actionUserId = str(self.get_argument('userId','0')) #action user
         targetUserId = str(self.get_argument('friendId','0')) #tagert user
         
         #if user ids are valid >0
         if int(actionUserId) != 0 and int(targetUserId) !=0 and int(actionUserId) != int(targetUserId):
         
             #get user emails   
             aEmail = str(uMgr.getUserEmail(actionUserId)) #action user email
             tEmail = str(uMgr.getUserEmail(targetUserId) )#targeted user email

             #if emails are not empty 
             if aEmail != '' and tEmail != '':
                 
                 # make friendship request
                 isMade = fMgr.sendFriendRequestFRS(aEmail,tEmail)

                 #if friend request made
                 if isMade==True:
                     #accepted
                    self.set_status(202) 
                    self.write('200')

                 else: 
                     #exception occured   
                    self.set_status(417) 
                    self.write('417')   
                              
             else:#either one email is/are empty
                self.set_status(404) 
                self.write('404')            

         else: #invalid user ids
            self.set_status(404)
            self.write('404')
       
#return all sent and pending friend requests by user
class AllPendingRequestsFRS(tornado.web.RequestHandler):

    def get(self):

        #filter user id from request
        userId = str(self.get_argument('userId','0'))

        #check userId is in request
        if int(userId)>0:
            
            #create UserManager object
            uMgr = UserManagerFRS()
            fMgr = FriendshipManagerFRS()

            #get email of user
            email = uMgr.getUserEmail(userId)
            if email != '':
                #get all pending request
                response = str(fMgr.getAllPendingFriendRequests(email))
                self.write(response)
            else:
                #email not found
                self.write(404)
                self.write('')

        else:
            #bad request
            self.set_status(400)
            self.write('') 

#return all recieved requests
class AllRecievedRequestsFRS(tornado.web.RequestHandler):
    
        def get(self):

            #get user id from request
            userId = str(self.get_argument('userId','0'))
            
            #check user id is valid >0
            if int(userId) > 0:

                #invoke Data Access
                uMgr =UserManagerFRS()
                fMgr =FriendshipManagerFRS()

                #get email 
                email = uMgr.getUserEmail(userId)
                
                # if validate email not empty
                if email != '':
                        #get request list
                    requestSet =str( fMgr.getAllFriendRecievedRequests(email))
                    self.write(requestSet)

                else:
                    #bad request
                    self.set_status(400) 
                    self.write('')

            #user id invalid
            else:
                #bad request
                self.set_status(400)
                self.write('')
                 

#accept a recieved  friend request
class AcceptFriendRequestFRS(tornado.web.RequestHandler):

    def get(self):

        # filter userId and friendId
        userId = str(self.get_argument('userId','0'))
        friendId = str(self.get_argument('friendId','0'))
        type = str(self.get_argument('type','0'))

        #check ids are empty
        if userId !='0' and friendId !='0' and type!='0':

            #invoke data access
            uMgr =UserManagerFRS()

            #get emails
            userEmail = uMgr.getUserEmail(userId)
            friendEmail = uMgr.getUserEmail(friendId)

            #check emails
            if userEmail !='' and friendEmail != '':

                #create FriendMgr. object
                fMgr = FriendshipManagerFRS()

                #build new friendship ; friend-sender, user-reciver
                status = fMgr.acceptNewFriendshipFRS(friendEmail,userEmail,type)

                #build new friend relationship
                if status == True:
                    self.set_status(200)
                    self.write('200')

                else:
                    #exception failed
                    self.set_status(417) 
                    self.write('417')

            else:#no emails
                self.set_status(404)
                self.write('404')

        else:#no user ids
            self.set_status(404)
            self.write('404')


#Reject friend request 
class RejectFriendRequestFRS(tornado.web.RequestHandler):
    
    def get(self):

        # filter userId and friendId
        userId = str(self.get_argument('userId','0'))
        friendId = str(self.get_argument('friendId','0'))

        #check ids are empty
        if userId !='0' and friendId !='0':

            #invoke data access
            uMgr =UserManagerFRS()

            #get emails
            userEmail = uMgr.getUserEmail(userId)
            friendEmail = uMgr.getUserEmail(friendId)

            #check emails
            if userEmail !='' and friendEmail != '':

                #create FriendMgr. object
                fMgr = FriendshipManagerFRS()

                #remove existing relation request
                status = fMgr.cancelFriendRequestFRS(friendEmail,userEmail)

                #build new friend relationship
                if status == True:
                    self.set_status(200)
                    self.write('200')

                else:
                    #exception failed
                    self.set_status(417) 
                    self.write('417')

            else:#no emails
                self.set_status(404)
                self.write('404')

        else:#no user ids
            self.set_status(404)
            self.write('404')

 


#return all friends of user 
class AllFriendsOfUserFRS(tornado.web.RequestHandler):
    
    def get(self):

            #get user id from request
            userId = str(self.get_argument('userId','0'))
            
            #check user id is valid >0
            if int(userId) > 0:

                #invoke Data Access
                uMgr =UserManagerFRS()
                fMgr =FriendshipManagerFRS()

                #get email 
                email = uMgr.getUserEmail(userId)
                
                # if validate email not empty
                if email != '':
                    
                    #get request list
                    resultSet = str(json.dumps(fMgr.selectAllFriends(email)))
                    print "view-all-users request from "+ str(userId)+" at "+str(datetime.datetime.today())
                    if resultSet!= '0':
                        self.set_status(200)
                        self.write(resultSet)
                    else:
                        self.set_status(200)
                        self.write('[{"email":"No friends found","firstname":"Sorry"}]')


                else:
                    #bad request
                    self.set_status(400) 
                    self.write('[{"email":"No friends found","firstname":"Sorry"}]')

            #user id invalid
            else:
                #bad request
                self.set_status(400)
                self.write('[{"email":"No friends found","firstname":"Sorry"}]') 



#Search Friends/Users
class FindUserRequestFRS(tornado.web.RequestHandler):
    
    def get(self):
        
        #clear console
        clear = lambda: os.system('cls')
        clear()
        #filter user id
        key = str(self.get_argument('key',''))
        userId =str(self.get_argument('userId','0'))
        
        #if key id  is not empty
        if key != '' and int(userId) >0:
            
            #invoke data access
            uMgr = UserManagerFRS()
            results = uMgr.searchUser(str(key),userId)

            
            #convert results to json
            results =json.dumps(results)
                 
            self.write(results)
          

            

        else:
           self.set_status(417)
           self.write('')

#check the status of relationship between two users
class GetRelationshipStatusFRS(tornado.web.RequestHandler):
    
    def get(self):

        #get ids from request
        userId  = str(self.get_argument('userId','0'))
        candidateId = str(self.get_argument('friendId','0'))

        #if user id and can. id is valid
        if userId != '0' and candidateId != '0':

            #invoke Data Access
            uMgr = UserManagerFRS()
            
            #get emails
            userEmail =  uMgr.getUserEmail(userId)
            candEmail = uMgr.getUserEmail(candidateId)

            #if emails are valid
            if userEmail != '' and candEmail != '':
                
                #check relationship
                fMgr = FriendshipManagerFRS()

                #check is a friend
                isFriend = fMgr.checkExistingFriendshipFRS(userEmail,candEmail)
                # has sent a  friend request
                isRequested  = fMgr.checkExistingRequestFRS(userEmail,candEmail)

                #if user is a friend
                if isFriend == 1:
                        self.write('F')
                        self.set_status(200)
                elif isFriend == 0:
                        if isRequested ==1:
                            self.write('R')
                            self.set_status(200)   
                        elif isRequested == 0:
                            self.write('N')
                            self.set_status(200)

                        
                


            #invalid emails
            else:
                self.set_status(404)
                self.write('')
        
        #invalid user ids
        else:
            self.set_status(400)
            self.write('')

#remove existing relationship
class RemoveFriendshipFRS(tornado.web.RequestHandler):

    def get(self):
        #get ids from request
        userId  = str(self.get_argument('userId','0'))
        candidateId = str(self.get_argument('friendId','0'))

        if userId!='0' and candidateId!='0': #if userId's are valid
           
            #get emails
            uMgr =UserManagerFRS()

            userEM= uMgr.getUserEmail(str(userId))
            friendEM=uMgr.getUserEmail(str(candidateId))

            #check emails
            if userEM!='' and friendEM!='':
                fMgr =FriendshipManagerFRS()

                #remove friendship
                removal = fMgr.removeExistingRelationship(userEM,friendEM) 
                if removal ==True:
                    self.write('1')
                    self.set_status(200)
                elif removal ==False:
                    self.set_status(417)
                    self.write('0')

            else: 
                self.set_status(404)
                self.write('0')




        else:

            self.set_status(400)
            self.write('0')

  #authenticate user
class SignInFRS(tornado.web.RequestHandler):
    
    def get(self):
          
        userEmail  = str(self.get_argument('email',''))
        password = str(self.get_argument('password',''))
        print(' Signing in userEmail:%s password: %s'%(userEmail,password))
        validation ='-1'
        uMgr  = UserManagerFRS()

        validation = uMgr.validateUserCredentials(userEmail,password)
        print('credentails-validity:'+str(validation))
        status = False

        if validation[0]['userId']!='-1': #set user to online state
            status = uMgr.setUserOnline('T',userEmail)
        print ('Authentication:'%(str(status)))

        if status ==True  and validation[0]['userId']!='-1':
            self.write(json.dumps(validation))
            self.set_status(200)
        else:
            
            self.write(json.dumps(validation))
            self.set_status(200)

#sign out user

class SignOutUserFRS(tornado.web.RequestHandler):

    def get(self):
        userEmail  = str(self.get_argument('email',''))
        userId = str(self.get_argument('userId',''))
        
        uMgr  = UserManagerFRS.UserManagerFRS()

        dbUserId =uMgr.getUserId(userEmail)
        status = False
        if str(dbUserId) == userId:
            status = uMgr.setUserOnline('F',userEmail)

            if status == True :
                print (' user '+userId+" went offline")
                self.write(str(1))
                self.set_status(200)
            else:
                print (' unable to set user'+userId+' offline')
                self.write(str(-1))
                self.set_status(200)
        else:
            print (' invalid user to sign out uid:'+userId+'\n returning -1 ')
            self.write(str(-1))
            self.set_status(404)

#check user is online / offline

class CheckUserActiveStateFRS(tornado.web.RequestHandler):

    def get(self):

        userEmail  = str(self.get_argument('email',''))
        
        uMgr =UserManagerFRS()

        status  = uMgr.checkUserIsOnline(userEmail)
        
        if status != 'NA':
            self.write(status)
            self.set_status(200)
        else :
            self.write(status)
            self.set_status(404)

#create CHATTING relationship between set of users
class SendJoinChatRequestsFRS(tornado.web.RequestHandler):

    def post(self): #make chat requests

        data = json.loads(str(self.request.body))
        list ={}
        for slot in  data:
              
            list.update({str(slot):str(data[slot])})
        

        friends = str(list['friends'])
        friends = ast.literal_eval(friends)
        user =list['userId']
        cMgr = ChatManagerFRS()
        result= cMgr.sendJoinChatRequestFRS(user,friends)

        self.write(str(result))
        self.set_status(200)


#Remove CHATTING relationship between set of users
class EndChatRequestsFRS(tornado.web.RequestHandler):
    def get(self): #make chat requests

        userId = str(self.get_argument('userId','0'))
        if userId != '0':
            cMgr = ChatManagerFRS()
            cMgr.deleteExistingChatSession(userId)

            self.write('1')
            self.set_status(200)
        else:
            self.write('-1')
            self.set_status(200)       

#Show All chat requests where User nodes with CHATTING relationship in db
class ShowAllChatRequstsFRS(tornado.web.RequestHandler):
        def get(self):

            #get arguments
            userId  = str(self.get_argument('userId','0'))
            
            if userId !='0':
                cMgr = ChatManagerFRS()
                result=cMgr.getAllRecivedChatRequests(userId)
                
                self.write(json.dumps(result))
                self.set_status(200)
            else:
                self.write('[{"userId":"-1"}]')
                self.set_status(200)

#remove  :CHATTING relationship between specific two users
class RemoveFromChatFRS(tornado.web.RequestHandler):
    
    def delete(self):

        #get arguments
        userId = str(self.get_argument('userId','0'))
        ownerId  = str(self.get_argument('friendId','0'))

        #validate inputs
        try:
            if int(userId)>0 and int(ownerId)>0:
            
                cMgr = ChatManagerFRS()
                status =cMgr.rejectChatRequest(ownerId,userId)
                self.set_status(200)
                self.write(status)
            
            else:
                self.set_status(417)
                self.write('-1')

        except Exception ,e:
            
            print e.message
            self.set_status(417)
            self.write('-1')

#control of influences
class ChangeOpinionPriorityLevelFRS(tornado.web.RequestHandler):

    def post(self):
      
          
            #create request body dictionary
            requestBody ={}

            #get userId
            requestBody['userId'] = str(self.get_argument('userId','-1'))

            #get opinion parameters
            requestBody['opinions']= str(self.get_argument('opinions','-1'))
          

            #get preference parameters
            requestBody['quality'] = str(self.get_argument('quality','-1'))
            requestBody['brand'] = str(self.get_argument('brand','-1'))
            requestBody['price'] = str(self.get_argument('price','-1'))
            requestBody['color'] = str(self.get_argument('color','-1'))


            
            #update database using  data in requestBody dictionary
            uMgr = UserManagerFRS()
            updateSts = uMgr.updateInfluenceControlLevels(requestBody)

            if updateSts == '1': #update is success

                self.set_status(200)
                self.write(updateSts)

            else:
                self.set_status(417)
                self.write('-1')

     
#load default user preferences
class LoadOpinionPriorityLevelFRS(tornado.web.RequestHandler):

     def get(self):

         #get arguments
         userId =self.get_argument('userId','-1')

         if userId == '-1': #invalid argument as userId
             
             self.set_status(417)
             print 'invalid user id  at ChangeOpinionPriorityLevelFRS class'
             self.write('-1')

         else: #valid user id
            uMgr =UserManagerFRS()
            preferenceData = json.dumps(uMgr.getPersonalPreferences(userId))
            self.set_status(200)
            self.write(preferenceData)
            


             






#GMI Classes Starts
class ChartServiceGMI(tornado.web.RequestHandler):

    def get(self):

       
        productId = 0
        productId = int(self.get_argument('pid','0')) #get arguments
        coodinates = None

        #call data access method
        popularityGMI =GetPopularityDetailsGMI()
        coodinates = popularityGMI.getPopularityValues(productId)

        coodinates = json.dumps(coodinates) #convert to json
        print coodinates
        self.set_status(200)
        self.write(coodinates)

#create graph coodinates
class GraphServiceGMI(tornado.web.RequestHandler):

    def get(self):
        
        productId = 0
        productId = int(self.get_argument('pid','0')) #get arguments
        coodinates =[]

        #call data access method
        coodinates.append(GetPopularityDetailsGMI().getCountryWisePopularityValues(productId))

        coodinates = json.dumps(coodinates) #convert to json
        print coodinates
        self.set_status(200)
        self.write(coodinates)

#Get More Details    
class MoreInfoServiceGMI(tornado.web.RequestHandler):

    def get(self):

       
        productId = 0
        productId = int(self.get_argument('pid','0')) #get arguments
        coodinates =[]

        #call data access method
        coodinates.append(GetPopularityDetailsGMI().getProductValues(productId))

        coodinates = json.dumps(coodinates) #convert to json
        print coodinates
        self.set_status(200)
        self.write(coodinates)       
        
           
#Testing service-print the post  request body in console
class VerifyRequest(tornado.web.RequestHandler):

    def post(self):
         data=json.loads(str(self.request.body))
         print 'server response for POST'
         print'\n========\n'
         print data
         print'\n========'
         self.write('1')
         self.set_status(200)

    def get(self):
         data= str(self.get_argument('userId',''))
         print 'server response for GET'
         print'\n========\n'
         print data
         print'\n========'
         self.write('1')
         self.set_status(200)


class BeforeCartAPI(tornado.web.Application):
    def __init__(self):
        handlers = [




            #test post your request body
                (r"/beforecart/frs/test/?",VerifyRequest),

            #PRS Service handlers
                (r'/api/suggestions', SuggestionHandlerPRS, dict(db=db)),
                (r'/api/updateSuggestions', UpdateHandlerPRS, dict(db=db)),

            #FRS service handlers
                
                (r"/beforecart/frs/chatlist/?",SuggestFriendsForChatFRS),
                (r"/beforecart/frs/newfriends/?",SuggestNewFriendsFRS),
                (r"/beforecart/frs/savechat/?",saveNewChatFRS),
                (r"/beforecart/frs/createuser/?",CreateUserFRS),
                (r"/beforecart/frs/sendrequest/?",SendFriendRequestFRS),
                (r"/beforecart/frs/sentrequests/?",AllPendingRequestsFRS),
                (r"/beforecart/frs/recievedrequests/?",AllRecievedRequestsFRS),
                (r"/beforecart/frs/allfriends/?",AllFriendsOfUserFRS),
                (r"/beforecart/frs/acceptrequest/?",AcceptFriendRequestFRS),
                (r"/beforecart/frs/rejectrequest/?",RejectFriendRequestFRS),
                (r"/beforecart/frs/finduser/?",FindUserRequestFRS),
                (r"/beforecart/frs/removeuser/?",RemoveUserFRS),
                (r"/beforecart/frs/checkrelstatus/?",GetRelationshipStatusFRS),
                (r"/beforecart/frs/isonline/?",CheckUserActiveStateFRS),
                (r"/beforecart/frs/updateuser/?",UpdateUserDetailsFRS),
                (r"/beforecart/frs/unfriend/?",RemoveFriendshipFRS),
                (r"/beforecart/frs/updatefriends/?",UpdateAllFriendhipsFRS),
                (r"/beforecart/frs/signout/?",SignOutUserFRS),
                (r"/beforecart/frs/signin/?",SignInFRS),
                (r"/beforecart/frs/joinchat/?",SendJoinChatRequestsFRS),
                (r"/beforecart/frs/endchat/?",EndChatRequestsFRS),
                (r"/beforecart/frs/allchatrequests/?",ShowAllChatRequstsFRS),
                (r"/beforecart/frs/denychatrequest/?",RemoveFromChatFRS),
                (r"/beforecart/frs/loadpreferences/?",LoadOpinionPriorityLevelFRS),
               
                (r"/beforecart/frs/updatepreferences/?",ChangeOpinionPriorityLevelFRS),
                 
                

                
                #GMI URL

                (r"/beforecart/gmi/chart?",ChartServiceGMI),
                (r"/beforecart/gmi/map?",GraphServiceGMI),
                (r"/beforecart/gmi/more?",MoreInfoServiceGMI)               
            ]
        tornado.web.Application.__init__(self,handlers)




         

    #@ws.on('connect', namespace='/chat')
    #def connect():
    #    for x in last_messages:
    #        emit('message', x)


    #@ws.on('joined_message', namespace='/chat')
    #def joined_chat(data):
    #    emit('message', {'username': data['username'],
    #                     'dateTime': datetime.utcnow().isoformat() + 'Z',
    #                     'type': 'joined_message'},
    #         namespace='/chat', broadcast=True)


    #@ws.on('send_message', namespace='/chat')
    #def handle_message(data):
    #    res = {
    #        'message': data['message'],
    #        'username': data['username'],
    #        'dateTime': datetime.utcnow().isoformat() + 'Z',
    #        'type': 'message'
    #    }
    #    emit('message', res, namespace='/chat', broadcast=True)
    #    if len(res) > 100:
    #        last_messages.pop(0)
    #    last_messages.append(res)


def main():
#read settings from commandline
    #options.parse_command_line()
    #if options.init_db:
        #init_db(db)
    app = BeforeCartAPI() #application
    app.listen(8080)
    print ('server running on http://localhost:{}'.format(options.port))
    IOLoop.instance().start()



    
def init_db(db):
    try:
        db.create_collection('PRS')
    except:
        pass
    try:
        db['PRS'].insert({"id":0,"ready":"TRUE","suggestion_list":[1,2,3,4,5]}) # userId 0 is reserverd for the system.
    except:
        pass
    try:
        db['PRS'].update({'id':1}, {"$set":{"ready":"FALSE"}}, upsert=False)
    except:  
        pass
    db['PRS'].ensure_index('id', unique=True)
    db['PRS'].ensure_index('_id', unique=True)

if __name__ == '__main__':
       
        main()
       