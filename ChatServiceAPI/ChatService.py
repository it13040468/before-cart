from flask  import Flask,session
import os
from flask.ext.socketio import SocketIO,emit
import uuid

app = Flask(__name__,static_url_path='')

app.config['SECRET_KEY'] = 'secret!'


socketio =SocketIO(app)

messages  = [{'text':'Boot System','name':'Stevie'}]

users = {}

@socketio.on('connect',namespace = '/chat')
def makeConnection():
	session['uuid'] = uuid.uuid1()
	session['username'] = 'Kalana'
	print 'connected!'
		
	#update users  list in chat room
	users[session['uuid']] = {'username':'Kalana'}
	
	return 'chat started'

	
@app.route('/')
def mainIndex():
    print 'in hello world'
    return 'started!'

#launch the server

@app.route('/newpals/<string:userId>', methods=['GET'])
def suggestNewFriends(userId):
    return userId

if __name__ =='__main__':
	
	socketio.run(app ,host=os.getenv('IP','0.0.0.0'),port =int(os.getenv('PORT', 8080)), debug=True)
	
