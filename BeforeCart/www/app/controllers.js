﻿
(function () {
    "use strict";

    angular.module("myapp.controllers", ["ngCookies", "ngRoute"])
       /**
       *App controller
       *
       * @param {!angular.Scope} $scope
       * @constructor
       * @ngInject
       * @export
       **/

    .controller("appCtrl", ["$scope", function ($scope) {
        $scope.shouldShowDelete = false;
        $scope.shouldShowReorder = false;
        $scope.listCanSwipe = true
        $scope.data = {
            showDelete: false
        };

        /**
          * 
          * @param {Object} item
          * @param {number} fromIndex
          * @param {number} toIndex
          * @export
          */
        $scope.moveItem = function (item, fromIndex, toIndex) {
            $scope.items.splice(fromIndex, 1);
            $scope.items.splice(toIndex, 0, item);
        };

        /**
         * @param {Object} item
         * @export
         */
        $scope.onItemDelete = function (item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
        };

    }])

      /**
       *SignUp controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup
       * @constructor
       * @ngInject
       * @export
       **/
     .controller("signupCtrl", ["$ionicPopup", "$scope", "$state", '$http', function ($ionicPopup, $scope, $state, $http) {

         //SET DEFAULT IMAGE
         $scope.data.image = 'images/avatar.png'
         var username = $scope.data.username
         var password = $scope.data.password
         var confPwd = $scope.data.confPwd
         var firstName = $scope.data.firstName
         var lastName = $scope.data.lastName
         var email = $scope.data.email
         var phone = $scope.data.phone
         var address = $scope.data.address
         var postal = $scope.data.postal
         var state = $scope.data.state
         var image = $scope.data.image


         /**
         * CREATE USER ACCOUNT 
         * @export
         */
         $scope.signUpUser = function () {



             //GATHER DATA
             var username = $scope.data.username
             var password = $scope.data.password
             var confPwd = $scope.data.confPwd
             var firstName = $scope.data.firstName
             var lastName = $scope.data.lastName
             var email = $scope.data.email
             var phone = $scope.data.phone
             var address = $scope.data.address
             var postal = $scope.data.postal
             var state = $scope.data.state
             var image = $scope.data.image

             //SET IMAGE PATH
             console.log($scope.data.password)
             console.log($scope.data.firstName)
             console.log($scope.data.lastName)
             console.log($scope.data.email)
             console.log($scope.data.address)
             console.log($scope.data.image)


             //MAKE REQUEST
             $http({
                 url: 'https://research-thushan.c9users.io:8080/beforecart/frs/createuser/',
                 method: "POST",
                 params: { username: username, password: password, firstName: firstName, lastName: lastName, image: image, phone: phone, email: email, address: address, postal: postal, state: state },
                 headers: { 'Content-Type': 'text/html' }

             }).then(function (response) { //HTTP RESPONSE

                 console.log(response.data)
                 if (response.data == '-1' || response.data == '0') {

                     $ionicPopup.alert({
                         title: 'Sorry',
                         template: 'Something went wrong. Please try again'
                     });


                     console.error('error in new user creation')
                     console.error('CODE:' + response.data)
                     return false

                 }

                 if (response.data == '-2') {

                     $ionicPopup.alert({
                         title: 'Sorry!',
                         template: 'There is an existing account for given email address.Please change your given email address'
                     });

                     console.error('duplicate user creation blocked')
                     return false
                 }
                 else {

                     //STORE USER DATA
                     console.log('NEW_USERID' + response.data)
                     window.localStorage.setItem('userId', response.data)
                     window.localStorage.setItem('firstname', firstName)
                     window.localStorage.setItem('email', email)
                     window.localStorage.setItem('image', image)


                     //SET PREFERENCES ON CONFIRM
                     $ionicPopup.show({
                         title: 'Your account has been created',
                         template: '<p>We need few more details on your personal preferences.Do you want to adjust them now?</p>',
                         scope: $scope,
                         buttons: [{  //ADJUST PREFERNCES NOW
                             text: 'Yes',
                             type: 'button-positive',
                             onTap: function (e) {

                                 window.location = '#/app/stall'
                             }


                         },
                         {//ADJUST PREFERENCES LATER

                             text: 'Not now',
                             type: 'button-stable',
                             onTap: function (e) {
                                 window.location = '#/app/mainCatelog'
                             }
                         }
                         ]

                     });

                 }



             }, function (error) { //ERROR

                 console.error(error)

                 $ionicPopup.alert({
                     title: 'Sorry!',
                     template: 'The service is not available. Please try again later'
                 });
                 console.log('error in sending create user request')

             })

         }






         /**
        * SHOW PROFILE IMAGE GALLARY LIST
        * @export
        */
         $scope.showGallery = function () {

             //GALARY TEMPLATE
             var template = '<div class="list">' +

                            '<div class ="item item-avatar">' +
                            '<img src="images/avatar.png"/>' +
                            '<p>Default image</p>' +
                            '<ion-radio ng-model="data.profile" selected ng-value="\'default\'"></ion-radio>' +
                            '</div>' +

                            '<div  class ="item item-avatar">' +
                            '<img src="images/cordova.png"/>' +
                            '<p>Cordova</p>' +
                            '<ion-radio ng-model="data.profile" ng-value="\'cordova\'"></ion-radio>' +
                            '</div>' +

                            '<div  class ="item item-avatar">' +
                            '<img src="images/offline.png"/>' +
                            '<p>Red User</p>' +
                            '<ion-radio ng-model="data.profile" ng-value="\'red\'"></ion-radio>' +
                            '</div>' +

                            '<div  class ="item item-avatar">' +
                            '<img src="images/online.png"/>' +
                            '<p>Green User</p>' +
                            '<ion-radio ng-model="data.profile" ng-value="\'green\'"></ion-radio>' +
                            '</div>' +

                            '</div>'
             //SHOW PICK IMAGE POPUP
             $ionicPopup.show({
                 template: template,
                 title: 'Select profile picture',
                 scope: $scope,
                 buttons: [
                 { text: 'Cancel' },
                 {
                     text: 'OK',
                     type: 'button-positive',
                     onTap: function (e) {

                         if ($scope.data.profile == 'default') {

                             $scope.data.image = 'images/avatar.png'
                         }
                         else if ($scope.data.profile == 'cordova') {

                             $scope.data.image = 'images/cordova.png'
                         }
                         else if ($scope.data.profile == 'red') {

                             $scope.data.image = 'images/offline.png'
                         }
                         else if ($scope.data.profile == 'green') {

                             $scope.data.image = 'images/online.png'
                         }

                         else {
                             $scope.data.image = 'images/avatar.png'
                         }

                     }

                 }
                 ]
             })

         }


         $scope.refresh = function () {

             //refresh binding
             $scope.$broadcast("scroll.refreshComplete");
         };


     }])

        /**
       *Reset password  controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("resetPwdCtrl", ["$scope", "$state", function ($scope, $state) {


            /**
            * refresh
            * @export
            */
            $scope.refresh = function () {
                //refresh binding
                $scope.$broadcast("scroll.refreshComplete");
            };
        }])
        /**
       *Reset password controller
       *
       * @param {!angular.Scope} $scope ,$state
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("beforeResetPwdCtrl", ["$scope", "$state", function ($scope, $state) {

            /**
            * refresh
            * @export
            */
            $scope.refresh = function () {
                //refresh binding
                $scope.$broadcast("scroll.refreshComplete");
            };
        }])

       /**
       *Popularity controller
       *
       * @param {!angular.Scope} $scope ,$state
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("popularityCtrl", ["$scope", "$state", function ($scope, $state) {

            /**
            * refresh
            * @export
            */
            $scope.refresh = function () {
                //refresh binding
                $scope.$broadcast("scroll.refreshComplete");
            };
        }])

       /**
       *Finish purchase controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup
       * @constructor
       * @ngInject
       * @export
       **/
         .controller("finsihPurchaseCtrl", ["$scope", "$state", function ($scope, $state) {

             /**
             * referesh
             * @export
             */
             $scope.refresh = function () {
                 //refresh binding
                 $scope.$broadcast("scroll.refreshComplete");
             };
         }])


       /**
       *Stall controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup,$ionicPopup,$ionicLoading
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("stallCtrl", ['$rootScope', "$scope", '$http', "$state", "$cookieStore", '$ionicPopup', '$ionicLoading', function ($rootScope, $scope, $http, $state, $cookieStore, $ionicPopup, $ionicLoading) {

           /**
            *  check attrubute of suggested product
            * @param {int} value
            * @param {string} fallback
            * @export
            */
           function checkAttribute(value, fallback) {
               if (value !== undefined && value !== null && value.toString()) {
                   return value;
               }
               else {

                   return fallback;
               }
           }

           /**
          * UPDATE SUGGESTION HANDLER
          * @param {string} status
          * @param {Object} product
          * @param {string} type
          * @param {string} basket
          * @export
          */
           $scope.updateSuggestionHandler = function (status, product, type, bascket) {

               var userId = window.localStorage.getItem('userId')
               var wishlist = localStorage.getItem("wishlist")

               if (userId == null || userId == '' || userId == '-1') { return }


               var typeName = 'REMOVE';
               if (type) {
                   typeName = 'ADD'
               } else {
                   typeName = 'REMOVE'
               }

               $http({
                   url: 'https://research-thushan.c9users.io:8080/prs/update',
                   method: "GET",
                   params: {
                       'status': status,
                       'userID': userId,
                       'password': window.localStorage.getItem('password'),
                       'productID': checkAttribute(product.ID, 1),
                       'categoryID': checkAttribute(product.categoryID, 1),
                       'suggestionID': checkAttribute(product.suggestionID, 0),
                       'score': checkAttribute(product.score, 0),
                       'weight': checkAttribute(product.weight, 0),
                       'strength': checkAttribute(product.strength, 0),
                       //'status': checkAttribute(product.status, 'TRUE'),
                       'type': checkAttribute(typeName, 'REMOVE'),
                       'modelType': checkAttribute(product.modelType, 'REDUCED'),
                       'bascketType': checkAttribute(bascket, 'REDUCED'),
                       'suggestionType': checkAttribute(product.suggestionType, 'REDUCED')
                   },
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function (response) {

               })

               $http({
                   url: 'https://research-thushan.c9users.io:8080/api/updateSuggestions',
                   method: "GET",
                   params: {
                       'status': status,
                       'userId': userId

                   },
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function (response) {

               })
           }

           /**
           * THE IMPLEMENTATION OF WISHLIST 
           * @export
           */
           $scope.loadWishList = function () {

               console.log('loading the wish list to UI')

               if (JSON.parse(localStorage.getItem('wishlist')) != null) {
                   var wishlist = JSON.parse(localStorage.getItem('wishlist'))
                   $scope.wishlist = wishlist
                   $scope.$apply()
               }
               else { console.log('no products found at wishlist to load') }



           }

           $scope.loadWishList() //INITIATE  CONTROLLER ATTRIBUTES

           /**
           *  DELETE ITEM FRO WISHLIST
           * @param {Object} item
           * @export
           */
           $scope.deleteWishListItem = function (item) {

               $ionicPopup.show({
                   template: '<p>Are you sure to remove product from wish list?</p>',
                   title: 'Delete Products',

                   scope: $scope,
                   buttons: [
                     { text: 'No' },
                     {
                         text: 'Yes',
                         type: 'button-positive',
                         onTap: function (e) {

                             console.log('removing item from WISHLIST index-of ' + item)
                             var index = $scope.wishlist.indexOf(item);
                             $scope.wishlist.splice(index, 1);

                             //UPDATE MEMORY
                             var wishlist = JSON.parse(localStorage.getItem("wishlist"));
                             var tmpIndex = wishlist.indexOf(item)
                             wishlist.splice(tmpIndex, 1)

                             //REPLACE NEW LIST TO MEMORY
                             localStorage.setItem("wishlist", JSON.stringify(wishlist));

                         
                             $scope.updateSuggestionHandler('TRUE', item, false, 'WISHLIST')

                         }
                     }]
               })

           }

           /**
           * EXECUTE PURCHASEING PROCEDURE
           * @param {Object} product
           * @param {string} userId
           * @param {int} quantity
           * @export
           */
           $scope.executePurchase = function (product, userId, quantity) {

               console.log('Purchasing product...')
               $scope.updateSuggestionHandler('FALSE', product, true, 'CART')

               try {

                   //SEND PURCHASE PRODUCT REQUEST
                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/savepurchase/',
                       method: "POST",
                       params: { 'productId': product.ID, 'customerId': userId, 'quantity': quantity, 'type': product.TYPE },
                       headers: { 'Content-Type': 'text/html' }

                   }).then(

                   function (response) {

                       var result = response.data
                       if (result == '1') {



                           window.location = '#/app/finsihPurchase'
                       } else {

                           $ionicPopup.alert({
                               title: 'Sorry',
                               template: 'Something went wrong with your purchase.Please try again'
                           });
                       }

                   })

               }

               catch (Error) {

                   console.log(Error)
                   alert('Oops something went wrong during saving your purchase.Please try again')
                   return false

               }
               finally {



                   console.log('REMOVE PRODUCT FROM WISH LIST')
                   var index = $scope.wishlist.indexOf(product);
                   $scope.wishlist.splice(index, 1);

                   //UPDATE MEMORY
                   var wishlist = JSON.parse(localStorage.getItem("wishlist"));
                   var tmpIndex = wishlist.indexOf(product)
                   wishlist.splice(tmpIndex, 1)

                   //REPLACE NEW LIST TO MEMORY
                   localStorage.setItem("wishlist", JSON.stringify(wishlist));

                   return true

               }

           }

           /**
           * PURCHASE PRODUCT CONFIRMATION
           * @param {Object} product
           * @export
           */
           $scope.confirmPurchaseItem = function (product) {

               var userId = window.localStorage.getItem('userId')
               var isSaved = false
               //IF USER IS VALID
               if (userId != null) {

                   var quantity = 1
                   $scope.confirmPurchasePopup = $ionicPopup.show({
                       template: '<p>Do you want to purchase this product?</p>',
                       title: 'Purchase Product',

                       scope: $scope,
                       buttons: [
                         { text: 'No' },
                         {
                             text: 'Yes',
                             type: 'button-positive',
                             onTap: function (e) {


                                 $scope.executePurchase(product, userId, quantity)
                                 $scope.confirmPurchasePopup.close()

                             }
                         }]
                   })

                   //NO USER ID FOUND
               } else {

                   console.warning('CURRENT USER' + userId)
                   $ionicPopup.alert({
                       title: 'Sorry',
                       template: 'Unable to find your identity.Please Sign In again'
                   });
               }

           }

           /**
           * state changed event
           * @param {Event} event
           * @param {State} toState
           * @param {State} fromState
           * @param {Params} fromParams
           * @export
           */
           $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
               if (toState.name == 'app.stall') {
                   console.log('app.stall Triggered')

                   $scope.loadPreferences()
                   $scope.loadWishList()
               }

           })

           /**
           * load preferences
           * @export
           */
           $scope.loadPreferences = function () {
               console.log('stallCtrl is executing...')
               console.log('setting default select-box values...')

               //initaite default selections
               $scope.opinions = 'both'

               $scope.price = 3
               $scope.brand = 3
               $scope.color = 3
               $scope.quality = 3

               //loading persoanl preferences form database

               $scope.userId = window.localStorage.getItem('userId')

               console.log('loading personal preferences form database...')

               if ($scope.userId != null || $scope.userId != '-1' || $scope.userId != '0') { //valid user

                   console.log('laoding prefence values for user:' + $scope.userId)
                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/loadpreferences/', method: "GET", params: { userId: $scope.userId }
                   }).then(function (response) {

                       var resultData = response.data
                       //if server returned data of a valid user
                       if (resultData['userId'] != '-1') {

                           console.log('personal preference values are loaded from database. updating model...')

                           //replace personal prefrences values in model the values from  database
                           $scope.userId = resultData['userId']
                           $scope.opinions = resultData['opinions']
                           $scope.price = resultData['price']
                           $scope.brand = resultData['brand']
                           $scope.color = resultData['color']
                           $scope.quality = resultData['quality']

                           console.log('default user preferences have been replaced with new values.')

                       }
                       else { //laod default values

                           console.log('server responsed error for userId:' + $scope.userId)
                           $scope.experts = 50
                           $scope.family = 50
                           $scope.price = 3
                           $scope.brand = 3
                           $scope.color = 3
                           $scope.quality = 3

                       }

                   }, function (response) { console.log('server error.Unable to load user preferences remotly.See server console') })

               }
           }

           /**
           * update the favorations for relationship category 
           * @export
           */
           $scope.updateOpinionSettings = function () {

               console.log('excuting updateOpinionSettings() ...')

               //get arguments for opinions

               var opinionsVal = $scope.opinions

               //get arguments for  persoanl product attitudes

               var price = $scope.price
               var brand = $scope.brand
               var color = $scope.color
               var quality = $scope.quality

               var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')
               console.log('retreive arguments ...')


               //validate arguments and userId
               if ((opinionsVal != null && userId != '-1' && userId != '0') &&
                   (price != null && brand != null && color != null && quality != null)) {

                   console.log("opinions-are-wanted to this user from: " + opinionsVal)
                   console.log('price:' + price + " brand:" + brand + "color:" + color + " quality:" + quality)


                   //preparing http request   'http://localhost:8080

                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/updatepreferences/',
                       method: "POST",
                       params: { opinions: opinionsVal, userId: userId, brand: brand, price: price, quality: quality, color: color },
                       headers: { 'Content-Type': 'text/html' },

                   }).then(

                   function (response) { //success

                       console.log('server responded for updateOpinionSettings request')


                       if (response.data == '-1') {

                           console.log('transaction failed.see server console')
                           //dispaly alert  success
                           $ionicPopup.alert({
                               title: 'Somthing went wrong',
                               template: 'Your changes not been saved. Please try again'
                           });

                       }
                       else if (response.data == '1') {

                           console.log('transaction is success')

                           //display alert failure
                           $ionicPopup.alert({
                               title: 'Changes saved',
                               template: 'Your changes have been saved'
                           });

                       }

                   },
                   function (response) {  //error

                       console.log("ERROR: server responded an error to updateOpinionSettings request ")
                       console.log(response.data)
                       //display alert server-error

                       $ionicPopup.alert({
                           title: 'Sevice Unavailable',
                           template: 'BeforeCart failed to find the service. Please try again.'
                       });
                   }

                   )





               }
               else
                   console.log('ERROR: one or more inputs for updateOpinionSettings() are invalid')
               console.log("opinions-set:" + opinionsVal)
               console.log('price:' + price + " brand:" + brand + "color:" + color + " quality:" + quality)

           }

           /**
           * referesh
           * @export
           */
           $scope.refresh = function () {
               $scope.$broadcast("scroll.refreshComplete");
           }
       }])

       /**
       *Change Account controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup,$ionicPopup,$rootScope
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("changeAccountCtrl", ['$rootScope', '$ionicPopup', '$http', "$scope", "$state", function ($rootScope, $ionicPopup, $http, $scope, $state) {



           //SET DEFAULT IMAGE
           $scope.data.image = window.localStorage.getItem('image')

           if ($scope.data.image == null)
               $scope.data.image = 'images/avatar.png'

           //var username = $scope.data.username
           //var password = $scope.data.password
           //var confPwd = $scope.data.confPwd
           //var firstName = $scope.data.firstName
           //var lastName = $scope.data.lastName
           //var email = $scope.data.email
           //var phone = $scope.data.phone
           //var address = $scope.data.address
           //var postal = $scope.data.postal
           //var state = $scope.data.state
           //var image = $scope.data.image

           /**
           * LOAD USER PROFILE DATA SERVER  
           * @export
           */
           $scope.loadUserData = function () {

               var userId = window.localStorage.getItem('userId')
               if (userId == null || userId == '-1' || userId == '0') {
                   console.error('Unable to load user details.No userId found locally,found USER_ID:' + userId)
                   return false
               }
               $http({

                   url: 'https://research-thushan.c9users.io:8080/beforecart/frs/loaduser/',
                   method: "GET",
                   params: { id: userId }

               }).then(
               //SERVER RESPONDED
               function (response) {

                   var responseState = response.data.status
                   if (responseState == 'FALSE') {

                       console.error('server responded error. Unable to fetch user node data')

                   }
                   else if (responseState == 'TRUE') {

                       console.log('fetching user data to UI...')
                       var user = response.data.user
                       console.log(user)
                       console.log(user.username)
                       $scope.data.username = user.username
                       $scope.data.password = user.password
                       //$scope.data.confPwd = user.password
                       $scope.data.firstName = user.firstName
                       $scope.data.lastName = user.lastName
                       $scope.data.email = user.email
                       $scope.data.phone = user.phone
                       $scope.data.address = user.address
                       $scope.data.postal = user.postal
                       $scope.data.state = user.state
                       $scope.data.image = user.image
                       $scope.$apply()
                   }
               },
               // SERVER ERROR
               function (error) {


                   console.error('Unable to  contact server. Internal server error')
                   console.error(error)

               })

           }

           /**
           * LOAD USER PROFILE DATA SERVER  
           * @export
           */
           $scope.editAccountDetails = function () {

               //GATHER DATA
               var username = $scope.data.username
               var password = $scope.data.password
               //var confPwd = $scope.data.confPwd
               var firstName = $scope.data.firstName
               var lastName = $scope.data.lastName
               var email = $scope.data.email
               var phone = $scope.data.phone
               var address = $scope.data.address
               var postal = $scope.data.postal
               var state = $scope.data.state
               var image = $scope.data.image

               var userId = window.localStorage.getItem('userId')

               if (userId == null || userId == '') {

                   console.error('Invalid user id:' + userId)

               }

               //SET IMAGE PATH
               console.log($scope.data.password)
               console.log($scope.data.firstName)
               console.log($scope.data.lastName)
               console.log($scope.data.email)
               console.log($scope.data.address)
               console.log($scope.data.image)


               //MAKE REQUEST
               $http({
                   url: 'https://research-thushan.c9users.io:8080/beforecart/frs/updateuser/',
                   method: "POST",
                   params: { userId: userId, username: username, password: password, firstName: firstName, lastName: lastName, image: image, phone: phone, email: email, address: address, postal: postal, state: state },
                   headers: { 'Content-Type': 'text/html' }

               }).then(function (response) { //HTTP RESPONSE

                   console.log(response.data)
                   if (response.data == '-1' || response.data == '0') {

                       $ionicPopup.alert({
                           title: 'Sorry',
                           template: 'Something went wrong. Please try again'
                       });


                       console.error('error in new user creation')
                       console.error('CODE:' + response.data)
                       return false

                   }

                   if (response.data == '-2') {

                       $ionicPopup.alert({
                           title: 'Sorry!',
                           template: 'There is an existing account for given email address.Please change your given email address'
                       });

                       console.error('duplicate user creation blocked')
                       return false
                   }
                   else {

                       //STORE USER DATA
                       console.log('UPDATED_USERID' + response.data)
                       window.localStorage.setItem('userId', response.data)
                       window.localStorage.setItem('firstname', firstName)
                       window.localStorage.setItem('email', email)
                       window.localStorage.setItem('image', image)


                       $ionicPopup.alert({
                           title: 'Changes saved',
                           template: 'Your changes have been saved successfully'
                       })


                   }



               }, function (error) { //ERROR

                   console.error(error)

                   $ionicPopup.alert({
                       title: 'Sorry!',
                       template: 'The service is not available. Please try again later'
                   });
                   console.log('error in sending create user request')

               })

           }

           /**
           * SHOW PROFILE IMAGE GALLARY LIST  
           * @export
           */
           $scope.showGallery = function () {

               //GALARY TEMPLATE
               var template = '<div class="list">' +

                              '<div class ="item item-avatar">' +
                              '<img src="images/avatar.png"/>' +
                              '<p>Default image</p>' +
                              '<ion-radio ng-model="data.profile" selected ng-value="\'default\'"></ion-radio>' +
                              '</div>' +

                              '<div  class ="item item-avatar">' +
                              '<img src="images/cordova.png"/>' +
                              '<p>Cordova</p>' +
                              '<ion-radio ng-model="data.profile" ng-value="\'cordova\'"></ion-radio>' +
                              '</div>' +

                              '<div  class ="item item-avatar">' +
                              '<img src="images/offline.png"/>' +
                              '<p>Red User</p>' +
                              '<ion-radio ng-model="data.profile" ng-value="\'red\'"></ion-radio>' +
                              '</div>' +

                              '<div  class ="item item-avatar">' +
                              '<img src="images/online.png"/>' +
                              '<p>Green User</p>' +
                              '<ion-radio ng-model="data.profile" ng-value="\'green\'"></ion-radio>' +
                              '</div>' +

                              '</div>'
               //SHOW PICK IMAGE POPUP
               $ionicPopup.show({
                   template: template,
                   title: 'Select profile picture',
                   scope: $scope,
                   buttons: [
                   { text: 'Cancel' },
                   {
                       text: 'OK',
                       type: 'button-positive',
                       onTap: function (e) {

                           if ($scope.data.profile == 'default') {

                               $scope.data.image = 'images/avatar.png'
                           }
                           else if ($scope.data.profile == 'cordova') {

                               $scope.data.image = 'images/cordova.png'
                           }
                           else if ($scope.data.profile == 'red') {

                               $scope.data.image = 'images/offline.png'
                           }
                           else if ($scope.data.profile == 'green') {

                               $scope.data.image = 'images/online.png'
                           }

                           else {
                               $scope.data.image = 'images/avatar.png'
                           }

                       }

                   }
                   ]
               })

           }

           /**
           * state changed event
           * @param {Event} event
           * @param {State} toState
           * @param {State} fromState
           * @param {Params} fromParams
           * @export
           */
           $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {


               if (toState.name == 'app.changeAccount') {

                   $scope.loadUserData()


               }
           })

           /**
           * refresh
           * @export
           */
           $scope.refresh = function () {

               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };
       }])

       /**
       *Comment controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup,$rootScope
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("commentCtrl", ["$scope", "$state", "$ionicPopup", "$http", '$rootScope', function ($scope, $state, $ionicPopup, $http, $rootScope) {

           $scope.userId = window.localStorage.getItem('userId')
           $scope.productId = window.localStorage.getItem('productId')
           $scope.Comments = []

           /**
           * Load comments
           * @param {int} productId
           * @export
           */
           $scope.loadComments = function (productId) {

               $scope.Comments = []
               $scope.$apply()
               window.localStorage.setItem('productId', productId)
               $http({


                   url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/getcomments',
                   method: "GET",
                   params: { pid: productId }

               }).then(function (response) {

                   console.log(" server responded")

                   var result = response.data
                   console.log('printing result of loadComment()')
                   var item = 0
                   var firstname = window.localStorage.getItem('firstname')

                   for (var comment in result) {
                       if (result[comment].userName == firstname)
                           result[comment].userName = 'You'
                       console.log('NAME' + result[comment].userName)
                   }

                   $scope.Comments = result
               })

           }

           $scope.productId = window.localStorage.getItem('productId')
           $scope.loadComments($scope.productId)

           /**
           *add comment
           * @export
           */
           $scope.addComment = function () {
               $ionicPopup.show({
                   template: '<input type="text" placeholder="type your comment here" ng-model="data.comm">',
                   title: 'Add new comment',
                   subTitle: 'Add new comment',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Add</b>',
                         type: 'button-positive',
                         onTap: function (e) {
                             if (!$scope.data.comm) {

                                 e.preventDefault();
                             } else {



                                 var userId = window.localStorage.getItem('userId')
                                 var image = window.localStorage.getItem('image')
                                 var uname = window.localStorage.getItem('firstname')
                                 var productId = window.localStorage.getItem('productId')
                                 if (userId == null && image == null && uname == null && productId == null) {

                                     console.error('CANNOT SAVE COMMENT IN DATABASE .ONE OR ATTRIBUTES ARE MISSING')
                                     return
                                 }
                                 console.log('Ssaving chat in database...')

                                 $http({


                                     url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/addcomments',
                                     method: "POST",
                                     params: { pid: productId, uid: userId, uname: uname, image: image, comment: $scope.data.comm },
                                     headers: { 'Content-Type': 'text/html' }

                                 }).then(function (response) {

                                     if (response.data = '200') {
                                         console.log('Comment added')
                                     }
                                     else {

                                         console.error('New comment did not added')
                                     }


                                 })
                                 console.log('Refreshing comments list')
                                 $scope.loadComments(productId)
                                 //var maxId =0
                                 //  for (var item in $scope.Comments){

                                 //      if (maxId < $scope.Comments[item].id) {
                                 //          maxId = $scope.Comments[item].id
                                 //      }

                                 //  }


                             }
                         }
                     }
                   ]
               });


           }

           /**
           *show edit comment popup
           * @param {bbject} comment
           * @export
           */
           $scope.showEdit = function (comment) {
               var status = comment.status
               var commentId = comment.id

               $ionicPopup.show({
                   template: '<input type="text" placeholder="' + status + '" ng-model="data.comment">',
                   title: 'Edit comment',
                   subTitle: 'Change your comment',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Save</b>',
                         type: 'button-positive',
                         onTap: function (e) {
                             if (!$scope.data.comment) {

                                 e.preventDefault();
                             } else {
                                 $http({


                                     url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/updatecomments',
                                     method: "POST",
                                     params: { cid: commentId, ucomment: $scope.data.comment },
                                     headers: { 'Content-Type': 'text/html' }

                                 }).then(function (response) {

                                     var result = response.data


                                     if (result == '200') {

                                         console.log('COMMENT:' + $scope.data.comment)
                                         console.log('COMMENT_ID' + commentId)
                                         console.log('EDIT COMMENT RESPONSE:' + result)

                                         //location.reload()
                                         //$scope.loadComments(259)

                                     }


                                 })

                                 var newComment = $scope.data.comment
                                 var index = $scope.Comments.indexOf(comment)
                                 $scope.Comments.splice(index, 1)
                                 $scope.$apply()
                                 comment.status = newComment
                                 $scope.Comments.push(comment)
                                 $scope.$apply()
                             }
                         }
                     }
                   ]
               });

           }

           /**
           *delete comment
           * @param {bbject} comment
           * @export
           */
           $scope.deleteComment = function (comment) {

               var commentId = comment.id

               $ionicPopup.show({

                   title: 'Are you sure you want to delete this comment?',

                   scope: $scope,
                   buttons: [
                     { text: 'No' },
                     {
                         text: 'Yes',
                         type: 'button-positive',
                         onTap: function (e) {

                             $http({
                                 url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/removecomments',
                                 method: 'DELETE',
                                 params: { cid: commentId }
                             }).then(function (response) {

                                 var result = response.data
                                 var res = parseInt(result.toString(), 10)

                                 if (res > 0) {
                                     //location.reload()
                                     var index = $scope.Comments.indexOf(comment)
                                     console.log('Deleting comment in index:' + index)
                                     $scope.Comments.splice(index, 1)
                                     $scope.$apply()
                                     //$scope.loadComments(259)
                                 }

                             })



                         }
                     }
                   ]
               });

           }

           /**
           *refresh
           * @export
           */
           $scope.refresh = function () {
               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };

           /**
          * state changed event
          * @param {Event} event
          * @param {State} toState
          * @param {State} fromState
          * @param {Params} fromParams
          * @export
          */
           $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

               $scope.Comments = []

               if (toState.name == 'app.commentview') {

                   console.log('LOADING COMMENTS FOR PRODUCT... ')
                   var productId = window.localStorage.getItem('productId')

                   if (productId != null) {

                       $scope.loadComments(productId)
                   }
                   else {

                       console.error('NO PRODUCT ID FOUND TO LOAD COMMENT')
                   }

               }
           })
       }])

       /**
       *Chat client controller
       *
       * @param {!angular.Scope} $scope ,$state, $http,$ionicPopup,$ionicScrollDelegate,$ionicLoading
       * @constructor
       * @ngInject
       * @export
       **/
       .controller('chatclientCtrl', function ($scope, $timeout, $ionicScrollDelegate, $ionicPopup) {

           //initaite dependencies
           var socket = io.connect('https://chat-thushan.c9users.io/chat')
           var chatOwnerId = window.localStorage.getItem('chatOwner')
           var firstname = window.localStorage.getItem('firstname')
           var userId = window.localStorage.getItem('userId')
           var image = window.localStorage.getItem('image')
           // console.log(image)
           $scope.products = []
           $scope.showTime = true;
           var firstname = window.localStorage.getItem('firstname')// $cookieStore.get('firstname')
           var alternate, isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();


           //join user for the chat session
           var clientData = { 'userId': userId, 'name': firstname, 'ownerId': chatOwnerId }
           socket.emit('joinRoom', clientData)

           //update the user interface with missed conversation data
           //socket.emit('loadMissedData',userId) 

           /**
            * state changed event
            * @param {Object} msg
            * @export
            */
           socket.on('message', function (msg) {

               //detect end of chat signal has araived
               if (msg.token == "NULL") {

                   window.localStorage.removeItem('chatUsers') //get dependencies
                   window.localStorage.removeItem('chatOwner')
                   $scope.messages = []
                   $scope.products = []
                   $scope.$apply()
                   //socket.disconnect() //disconnect
                   window.location = '#/app/mainCatelog' //finish and alert
               }
               else {
                   console.log(msg)
                   var index = $scope.messages.indexOf(msg)
                   if (index == '-1') {
                       if (msg.name == firstname)
                           msg.name = 'You'
                       $scope.messages.push(msg)
                       $scope.$apply()
                   }
               }
           })

           //display the product in ui
           socket.on('showProduct', function (product) {
               console.log('Displaying uploaded product')
               $scope.products.push(product)
               $scope.$apply()
           })

           /**
            * send message to flask
            * @export
            */
           $scope.sendMessage = function () {
               alternate = !alternate;

               var d = new Date();
               d = d.toLocaleTimeString().replace(/:\d+ /, ' ');
               var userId = window.localStorage.getItem('userId')
               var tempMsg = { 'name': firstname, 'text': $scope.data.message, 'userId': userId, 'image': image }
               socket.emit('update', tempMsg)



               console.log('printing message queue...')
               console.log($scope.messages)

               //saved locally
               //$cookieStore.put('chatData', $scope.messages)

               delete $scope.data.message;
               $ionicScrollDelegate.scrollBottom(true);

           };

           /**
            * scroll up ios
            * @export
            */
           $scope.inputUp = function () {
               if (isIOS) $scope.data.keyboardHeight = 216;
               $timeout(function () {
                   $ionicScrollDelegate.scrollBottom(true);
               }, 300);

           };

           /**
            * scroll down ios
            * @export
            */
           $scope.inputDown = function () {
               if (isIOS) $scope.data.keyboardHeight = 0;
               $ionicScrollDelegate.resize();
           };

           /**
            * close cordova keyboard
            * @export
            */
           $scope.closeKeyboard = function () {
               cordova.plugins.Keyboard.close();
           };

           $scope.data = {};
           $scope.myId = window.localStorage.getItem('userId')  // $cookieStore.get('userId');
           $scope.messages = [];

           /**
            *exit from the chat session
            * @export
            */
           $scope.exitChat = function () {

               $ionicPopup.show({

                   title: "Exit chat session ",
                   subTitle: '<p>Are you sure to exit from this chat session?</p>',
                   scope: $scope,
                   buttons: [
                  { text: 'No' },
                  {
                      text: '<b>Yes</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                          console.log('disonnecting client...')

                          var client = { 'userId': userId }
                          socket.emit('removeClient', client)
                          // socket.disconnect()

                          console.log('removing chat owner:' + chatOwnerId) //ersaing chat owner id

                          window.localStorage.removeItem('chatOwner')

                          console.log('navigating to main-catelog.html page...') //navigating home
                          window.location = '#/app/mainCatelog'
                          // location.reload()



                      }
                  }]
               })



           }

           /**
           *refresh
           * @export
           */
           $scope.refresh = function () {


               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };
       })

       /**
       *Ask Friend controller
       *
       * @param {!angular.Scope}'$interval','$rootScope', '$http', "$scope", "$state",  '$ionicPopup','$timeout','$ionicScrollDelegate'
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("askFriendCtrl", ['$interval', '$rootScope', '$http', "$scope", "$state", '$ionicPopup', '$timeout', '$ionicScrollDelegate', function ($interval, $rootScope, $http, $scope, $state, $ionicPopup, $timeout, $ionicScrollDelegate) {

            $scope.selection = []; //chosen  friends for chat
            $scope.chatInvitations = [] //recieved chat invitations
            $scope.products = [] //define products list

            //define socket connection for Controller
            var socket = io.connect('https://chat-thushan.c9users.io/chat')

            $scope.showTime = true;
            var firstname = window.localStorage.getItem('firstname')// $cookieStore.get('firstname')
            var userId = window.localStorage.getItem('userId')
            var image = window.localStorage.getItem('image')
            var email = window.localStorage.getItem('email')
            var alternate, isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

            /**
             *  recieve message event
             * @param {Object} msg
             * @export
             */
            socket.on('message', function (msg) {

                console.log('MESSAGE INSERTED' + msg)

                // if (userId == msg.token) {
                if (msg.token != 'NULL') {
                    if (msg.name == firstname)
                        msg.name = 'You'

                    $scope.messages.push(msg)
                    $scope.$apply()
                    console.log(msg)
                    //alert('SAVE CHAT DATA:' + $scope.messages)

                    window.localStorage.setItem('chatData', JSON.stringify($scope.messages))
                }
            })

            /**
             *  recieve product event
             * @param {Object} product
             * @export
             */
            socket.on('showProduct', function (product) {
                console.log('Displaying uploaded product')
                $scope.products.push(product)
                $scope.$apply()
            })

            /**
             * send message to flask
             * @param {Object} msg
             * @export
             */
            $scope.sendMessage = function () {

                alternate = !alternate;
                var d = new Date();
                d = d.toLocaleTimeString().replace(/:\d+ /, ' ');
                var userId = window.localStorage.getItem('userId')
                var tempMsg = { 'name': firstname, 'text': $scope.data.message, 'userId': userId, 'image': image }
                socket.emit('update', tempMsg)
                console.log('printing message queue...')
                console.log($scope.messages)
                //saved locally
                //$cookieStore.put('chatData', $scope.messages)
                delete $scope.data.message;
                $ionicScrollDelegate.scrollBottom(true);

            };

            /**
             * send product message to flask
             * @export
             */
            $scope.postProductInChat = function () {

                //retive saved product object in memory and validate
                console.log('sending selected product...')
                var product = JSON.parse(localStorage.getItem("product"))

                if (product != null) {
                    console.log('sending product ' + product.UPC + 'to server...')
                    //create socket connection
                    socket.emit('uploadProduct', product)
                }
            }

            /**
            * scroll up chat view ios
            * @export
            */
            $scope.inputUp = function () {
                if (isIOS) $scope.data.keyboardHeight = 216;
                $timeout(function () {
                    $ionicScrollDelegate.scrollBottom(true);
                }, 300);

            };

            /**
            * scroll down chat view ios
            * @export
            */
            $scope.inputDown = function () {
                if (isIOS) $scope.data.keyboardHeight = 0;
                $ionicScrollDelegate.resize();
            };

            /**
            * close keyboard  for cordova
            * @export
            */
            $scope.closeKeyboard = function () {
                cordova.plugins.Keyboard.close();
            };

            $scope.data = {};
            $scope.myId = window.localStorage.getItem('userId')  // $cookieStore.get('userId');
            $scope.messages = [];

            /**
            * display product details popup
            * @export
            */
            $scope.showProduct = function () {

                //retive saved product object in memory and validate
                console.log('displaying selected product...')
                var product = JSON.parse(localStorage.getItem("product"))
                if (product != null) {

                    var productTemplate = '<div class="list card"><div class="item item-avatar" >' +
                                    '<img src="' + product.STALL_IMAGE + '">' +
                                    ' <h2>' + product.STALL_NAME + '</h2>' +
                                    '<p>' + product.STALL_ADDRESS + '</p></div>' +

                                     '<div class="item item-body"><img class="full-image" src="' + product.PRODUCT_IMAGE + '"><p>' + product.PRODUCT_DESC + '</p>' +
                                            '<h3 class ="subdued"><i class="ion-cash"></i> ' + product.PRODUCT_PRICE + '</h3>' +
                                            '<h3 class ="subdued"><i class="ion-email"></i><br> ' + product.STALL_EMAIL + '</h3>' +
                                            '<h3 class ="subdued"><i class="ion-android-call"></i> <br>' + product.STALL_PHONE + '</h3>' +
                                         '</div>' +

                                    '</div>'

                    $ionicPopup.alert({
                        title: 'Selected Product',
                        template: productTemplate
                    });

                } else { console.error('no product loaded for select chat users view'); return }

            }

            /**
            * replace blank  profile image with default image
            * @param {string} image
            * @export
            */
            $scope.resolveProfileImage = function (image) {

                if (image == null) {

                    image = 'images/avatar.png'
                    return image
                }
                if (image.toString() == 'None') {
                    console.log('Profile pic replaced with default')
                    image = 'images/avatar.png'
                    return image
                } else {
                    return image
                }


            }

            /**
            * load friends for chat
            * @export
            */
            $scope.loadFriendsForChat = function () {

                console.log('loadFriendsForChat() method invoked')
                console.log('fetching product-category-Id and current-user-Id...')
                //get user-id and product-cat-id
                var catId = window.localStorage.getItem('catId') //$cookieStore.get('catId')
                var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')

                //send request to get chat list
                console.log('sending http request to get chat list...')
                $http({
                    url: 'https://research-thushan.c9users.io:8080/beforecart/frs/chatlist/',
                    method: "GET",
                    params: { userId: userId, catId: catId }

                }).then(function (response) {

                    console.log(' SUGGESTED CHAT USER LIST RECIEVED')
                    var chatList = response.data

                    for (var chatUser in chatList) {

                        chatList[chatUser].image = $scope.resolveProfileImage(chatList[chatUser].image)
                    }


                    $scope.suggestedChatList = chatList //update ui


                    //var checkList = []      //save user ids in cookieStore
                    //for (var user in chatList) {
                    //checkList[user] = chatList[user].userId
                    //}


                })

            }

            /**
            * store selected users
            * @param {string} userId
            * @export
            */
            $scope.toggleSelection = function (userId) {

                var index = $scope.selection.indexOf(userId);

                if (index > -1) {
                    $scope.selection.splice(index, 1);
                }
                else {

                    $scope.selection.push(userId)

                }

            }

            /**
            * send join chat invitations and navigate chat room if success
            * @export
            */
            $scope.sendJoinInvitations = function () {

                //replace new user list fo chat session 

                window.localStorage.removeItem('chatUsers')
                window.localStorage.setItem('chatUsers', JSON.stringify($scope.selection))

                //extract chat owner and selected chat room friends
                var chatUsers = JSON.parse(window.localStorage.getItem('chatUsers'))//$cookieStore.get('chatUsers')
                var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')

                //validate eligibility to make http request
                if ($scope.selection.length != 0 && (userId != '' || userId != '-1' || userId != '0')) {

                    //prepare json statement
                    var requestData = []
                    var friends = []

                    for (var candidate in chatUsers) {
                        // alert('CANDIDIATES ' + candidate + ':ID' + chatUsers[candidate])
                        friends.push({ 'user': $scope.selection[candidate] })
                    }
                    // set json statement to request data
                    requestData = { 'userId': userId, 'friend': friends }


                    //prepare request ,include request data
                    var requestTemplate = {
                        method: 'POST',
                        url: 'https://research-thushan.c9users.io:8080/beforecart/frs/joinchat/',
                        headers: { 'Content-Type': 'application/json' },
                        data: requestData
                    }
                    //make http POST request
                    $http(requestTemplate).then(function (response) {
                        //get server response
                        var result = response.data

                        //if response ==1 
                        if (result == '1') {
                            console.log('SERVER RESPOND for create chat request: ${result}')
                            console.log(requestData)

                            //navigate view-chat.html
                            $scope.navigateChatView(true)
                            $scope.makeSocketChatInvitation()

                        }
                        else { //failed response
                            console.log('failure happend while creating CHATT relationships in db')



                        }

                    })


                }
                else { //if userId erased or no chat users being selected by user

                    if (chatUsers.length == 0) { //no chat users
                        $ionicPopup.alert({
                            title: 'Cannot Start Chat',
                            template: 'Please send at least one invitation to start chat'
                        });

                    }
                    if (userId != '' || userId != '-1' || userId != '0') { //invalid userid return from $cookieStore
                        console.log('user id is invalid. Unable to make create chat request' + userId)
                    }



                }

            }

            /**
            *make socket  chat request
            * @export
            */
            $scope.makeSocketChatInvitation = function () {

                var chatUsers = JSON.parse(window.localStorage.getItem('chatUsers'))


                for (var user in chatUsers) { //send initations for each friend
                    var requestData = { 'from': userId, 'to': chatUsers[user], 'email': email, 'firstname': firstname, 'image': image }

                    socket.emit('listenChatRequests', requestData)
                    console.log('chat request is sent in socket.')
                }

            }

            /**
            *display stored users
            * @param {string} status
            * @export
            */
            $scope.navigateChatView = function (status) {

                //window.localStorage.getItem('userId')$cookieStore.get('chatUsers').length
                if (JSON.parse(window.localStorage.getItem('chatUsers')).length > 0 && status == true && JSON.parse(window.localStorage.getItem('product').length > 0)) { //validate the selection

                    //establish new chat
                    // var socket = io.connect('https://chat-thushan.c9users.io/chat')
                    var userId = window.localStorage.getItem('userId')
                    if (userId != null && userId != '' && userId != '0' && userId != '-1') {
                        var userData = { 'ownerId': userId }
                        try {
                            socket.emit('createNewSession', userData)
                        }
                        catch (error) { console.log('ERROR in navigateChatView() ' + error); return null }
                        finally {
                            window.location = "#/app/chatView"; //start chat window
                        }

                    } else { console.log('Invalid user id') }

                } else { console.log('One or more requirements unable to navigate chat view') }


            }

            /**
            *finalize on going chat session and save chat data in server db
            * @export
            */
            $scope.concludeChatSession = function () {


                try {



                    //extract chat data from $cookieStore
                    var chatData = JSON.parse(window.localStorage.getItem('chatData'))//$cookieStore.get('chatData')
                    var chatUsers = JSON.parse(window.localStorage.getItem('chatUsers'))//$cookieStore.get('chatUsers')
                    var productId = window.localStorage.getItem('productId')
                    var catId = window.localStorage.getItem('catId')
                    var userId = window.localStorage.getItem('userId')//$cookieStore.get('userId')
                    var friends = []
                    for (var candidate in chatUsers) {

                        friends.push({ 'user': chatUsers[candidate] })
                    }
                    // set json statement to request data
                    var requestData = { 'userId': userId, 'friends': friends, 'chat': chatData, 'product': productId, 'catId': catId }
                    //logging
                    console.log('Printing requestData...')
                    console.log('PRODUCT_UPC:' + productId)
                    console.log('CHAT_DATA:')
                    console.log(chatData)
                    console.log('FRIENDS:')
                    console.log(friends)
                    console.log('OWNER_ID:' + userId)

                    console.log('preparing  http request ...')
                    var response = false
                    //prepare http request template to send
                    if (friends != [] && productId != null && userId != null) {
                        var requestTemplate = {
                            method: 'POST',
                            url: 'https://research-thushan.c9users.io:8080/beforecart/frs/savechat/',
                            headers: { 'Content-Type': 'application/json' },
                            data: requestData
                        }
                        //make http POST request
                        $http(requestTemplate).then(function (response) {

                            if (response.data == '1') {

                                console.log('chat details are saved in server db')
                                console.log(response.data)
                                response = true

                            }
                            else {

                                console.log('chat details are not saved properly in server db. Check server console.')
                                console.log('Chat data NOT saved:' + response.data)
                                response = false
                            }


                        })
                    } else {
                        console.log('One or more save chat dependencies are missing!!')
                        console.log('PRODUCT_UPC:' + productId)
                        console.log('CHAT_DATA:' + chatData)
                        console.log('FRIENDS:' + friends)
                        console.log('OWNER_ID:' + userId)
                        console.log('NO data saved in this conversation')
                        response = false
                    }






                }
                catch (error) {

                    console.log('Error occured during sending chat information to server in concludeChatSession()')
                    console.log(error)
                    console.log('saving chat details  might not happend; check mongo  db in mLab')
                    //alert('Something went wrong. Please try again')
                    console.log('Exception in $scope.concludeChatSession')
                    response = false
                }
                finally {
                    console.log('$scope.concludeChatSession() in ViewChatCtrl executed without any error')
                    return response
                }





            }


            /**
            *finish ongoing chat
            * @export
            */
            $scope.finishOngoingChat = function () {
                $ionicPopup.show({

                    title: "Finish chat session ",
                    subTitle: '<p>Do you want to finish the chat?</p>',
                    scope: $scope,
                    buttons: [
                   { text: 'Cancel' },
                   {
                       text: '<b>Finish</b>',
                       type: 'button-positive',
                       onTap: function (e) {

                           console.log('finalizing chat session')
                           var finished = $scope.concludeChatSession() // save chat data in remote mongo db via server
                           console.log('concludeChatSession RETURNED:')
                           if (finished == true) {

                               //releasing converstion & product data and involved users for chat locally
                               console.log('releasing converstion  & product data and involved users for chat locally...')
                               // $cookieStore.remove('chatData') 
                               window.localStorage.removeItem('chatData')

                               window.localStorage.removeItem('productId')
                               window.localStorage.removeItem('product')

                               //$cookieStore.remove('chatUsers')
                               window.localStorage.removeItem('chatUsers')

                               console.log('chat session data removed successfully')
                               console.log('disconnecting [:CHATTING] relationships in db ...')


                               //make http request to disconnect all [:CHATTING] rels for user

                               //get userId
                               var userId = window.localStorage.getItem('userId')//$cookieStore.get('userId')
                               if (userId != '' || userId != '-1' || userId != '0') { //validate user id

                                   //make  request
                                   $http({
                                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/endchat/',
                                       method: "GET",
                                       params: { userId: userId }
                                   }).then(function (response) {

                                       if (response.data == '1') { //:CHATTING relationships are removed for user in database
                                           console.log('Chat session is closed. All :CHATTING relationships are removed for USER:' + userId + ' successfully')
                                           console.log('Chat concluded. Saving chat data is completed.')


                                       }
                                       else {
                                           console.log(' one or more :CHATTING relationships are FAILED to REMOVE of user%{userId} ')

                                       }

                                   });

                               }
                               else {
                                   console.log(' $scope.finishOngoingChat() invalid userId: ${userId}')
                               }


                           }

                           else {

                               //PRINT ERROR
                               console.error('$scope.concludeChatSession() returned FALSE')

                           }


                           console.log('Ending Chat session in Chat server...')
                           // Conclude chat session in SocketIO
                           console.log('Concluding Chat session in Chat Server..')

                           var name = window.localStorage.getItem('firstname')
                           var userId = window.localStorage.getItem('userId')
                           if (userId != null || firstname != null) {
                               var concludeData = { 'ownerId': userId, 'roomKey': userId, 'name': name }
                               console.log(concludeData)
                               socket.emit('conclude', concludeData)
                               //window.localStorage.removeItem('userId')
                               // window.localStorage.removeItem('email')
                               //window.localStorage.removeItem('firstname')
                               window.localStorage.removeItem('chatOwner')
                               window.localStorage.removeItem('chatUsers')
                               window.localStorage.removeItem('chatData')
                               window.localStorage.removeItem('productId')
                               //window.localStorage.removeItem("wishlist")
                               window.location = '#/app/mainCatelog'
                               window.location.reload()
                           }
                           else {
                               alert('Chat session cannot stoped .Your identity information has been erased from Before Cart locally. Please sign in')
                               cosnole.log(userId)
                               console.log(firstname)
                               console.log('+++++')
                               console.log(window.localStorage.getItem('userId'))
                               console.log(window.localStorage.getItem('email'))
                               console.log(window.localStorage.getItem('firstname'))
                               console.log(JSON.parse(window.localStorage.getItem('chatUsers')))
                               console.log(JSON.parse(window.localStorage.removeItem('chatData')))



                           }
                           //socket.disconnect()



                       }
                   }]
                })

            }

            /**
           * state changed event
           * @param {Event} event
           * @param {State} toState
           * @param {State} fromState
           * @param {Params} fromParams
           * @export
           */
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                //892
                if (fromState.name == 'app.mainCatelog') {
                    //&& fromState =='app.mainCatelog' toState.name == 'app.askfriend' && 
                    console.log('RELOAD CHAT LIST')
                    $scope.suggestedChatList = []
                    $scope.selection = []

                    $scope.loadFriendsForChat()
                    $scope.$apply()
                }

            })
            $scope.refresh = function () {
                //refresh binding
                $scope.$broadcast("scroll.refreshComplete");
            };
        }])

       /**
       *Find friends controller
       *
       * @param {!angular.Scope} $ionicLoading, $rootScope, $scope, $state, $http, $cookieStore, $ionicPopup, $route, $timeout, myappService'
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("findFriendsCtrl", ["$ionicLoading", "$rootScope", "$scope", "$state", "$http", '$cookieStore', '$ionicPopup', '$route', '$timeout', 'myappService', function ($ionicLoading, $rootScope, $scope, $state, $http, $cookieStore, $ionicPopup, $route, $timeout, myappService) {

           /**
           *SEARCH NEW USERS FOR FRIENDSHIP
           * @param {string} key
           * @export
           */
           $scope.searchForFriends = function (key) {

               var result = []
               $scope.searchFriendList = []

               var userId = window.localStorage.getItem('userId')
               if (userId == '' || userId == null || userId == '-1') {
                   console.error('invalid userId in local.searchForFriends()')
               }
               $scope.searchUserResultNotice = ''
               $http({
                   url: 'https://research-thushan.c9users.io:8080/beforecart/frs/searchusers/',
                   method: "GET",
                   params: { 'key': key, 'id': userId },

               }).then(function (response) {

                   result = response.data
                   console.log(result)
                   if (result.status == '1') {

                       if (result.results != null) {

                           //REPLACE EMPTY PROFILE PICTURES WITH DEFAULT IMAGE
                           for (var item in result.results) {
                               result.results[item].image = $scope.resolveProfileImage(result.results[item].image)
                           }

                           console.log(result.results)
                           $scope.searchFriendList = result.results
                           $scope.$apply()

                       }
                       else { $scope.searchUserResultNotice = 'Sorry,no users found!' }

                   } else {
                       $scope.searchUserResultNotice = 'Sorry,no users found!'
                       console.error('somthing wrong in response at searchForFriends.see server console ')

                   }

               })

               $scope.$apply()
           }

           /**
           *REMOVE THE OBJECT FROM searchFriendList LIST
           * @param {Object} item
           * @export
           */
           $scope.onItemDelete = function (item) {
               $scope.searchFriendList.splice($scope.searchFriendList.indexOf(item), 1);
           }

           /**
           *REPLACE BLANK IMAGE
           * @param {string} image
           * @export
           */
           $scope.resolveProfileImage = function (image) {

               if (image == null) {

                   image = 'images/avatar.png'
                   return image
               }
               if (image.toString() == 'None') {
                   console.log('Profile pic replaced with default')
                   image = 'images/avatar.png'
                   return image
               } else {
                   return image
               }


           }

           /**
           *SEND FRIEND REQUEST
           * @param {Object} suggestedFriend
           * @export
           */
           $scope.sendFriendRequestAtSearch = function (suggestedFriend) {
               $ionicPopup.show({

                   title: "Send request to " + suggestedFriend.firstname,
                   subTitle: '<p>Are you sure to  send this request?</p>',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Send</b>',
                         type: 'button-positive',
                         onTap: function (e) {

                             // alert("selected: " + $scope.data.relsts + "; fid: " + suggestedFriend.userId);
                             var friendId = suggestedFriend.userId
                             var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')

                             if (userId != '-1' && userId != '0' && friendId != '-1' && friendId != '0' && friendId != '' && userId != '') //validate user ids
                                 //send requests
                                 $http({ url: "https://research-thushan.c9users.io:8080/beforecart/frs/sendrequest/", method: 'GET', params: { 'userId': userId, 'friendId': friendId } }).then(function (response) {
                                     var response = response.data

                                     if (response == '200') {
                                         $ionicPopup.alert({
                                             title: 'Request is Sent',
                                             template: 'Your request has successfully made'
                                         });
                                         //refreshing ui

                                         //$scope.errorNewFriendsMsg = null
                                         $scope.onItemDelete(suggestedFriend)

                                     }
                                     else {
                                         $ionicPopup.alert({
                                             title: 'Ooops',
                                             template: 'Your request could not be saved'
                                         });
                                     }
                                 })


                         }
                     }
                   ]
               });
           }

           /**
           *REFRESH
           * @export
           */
           $scope.refresh = function () {
               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };
       }])

       /**
       *Main catelog controller
       * @param {!angular.Scope}$ionicLoading,$rootScope, $scope, $state, $http, $cookieStore, $ionicPopup, $route, $timeout, myappService
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("mainCatelogCtrl", ["$ionicLoading", "$rootScope", "$scope", "$state", "$http", '$ionicPopup', '$route', '$timeout', 'myappService', function ($ionicLoading, $rootScope, $scope, $state, $http, $ionicPopup, $route, $timeout, myappService) {



           var socket = io.connect('https://chat-thushan.c9users.io/chat')
           $scope.friendRequests = []
           $scope.chatRequests = []
           $scope.searchedProducts = []
           $scope.searchFriendList = []
           $scope.data.category = 'a'

           $scope.searchUserResultNotice = ''

           /**
           *UPDATE THE MAIN-CATELOG PAGE WITH SUGGESTED PRODUCTS LIST FROM LOCAL STORAGE
           * @export
           */
           $scope.showSuggestedProductList = function () {

               //var suggestedList = [   //FILL THIS  WITH JSON RESPOND
               //  { 'ID': '1', 'UPC': '259', 'ICOLOR': '', 'style': {},'COMMENTS': '3', 'LIKES': '2', 'STALL_NAME': 'Twitterworks', 'STALL_ADDRESS': '6517 Bartelt Circle', 'STALL_PHONE': '51-(567)644-3039', 'STALL_IMAGE': 'https://robohash.org/enimnequea.png?size=50x50&set=set1', 'STALL_EMAIL': 'cgomez1@theatlantic.com', 'PRODUCT_IMAGE': 'http://slimages.macys.com/is/image/MCY/1705769', 'CAT_ID': '2', 'PRODUCT_PRICE': '$800.00', 'PRODUCT_DESC': "LAUREN RALPH LAUREN DRESS, LONG-SLEEVE LACE SEQUIN NAVY 4",'TYPE': 'GLOBAL' },
               //  { 'ID': '3', 'UPC': '261', 'ICOLOR': '', 'style': {}, 'COMMENTS': '6', 'LIKES': '5', 'STALL_NAME': 'Skippad', 'STALL_ADDRESS': '9 Waywood Hill', 'STALL_PHONE': '81-(241)143-9402', 'STALL_IMAGE': 'https://robohash.org/quiliberocupiditate.png?size=50x50&set=set1', 'STALL_EMAIL': 'jholmes2@plala.or.jp', 'PRODUCT_IMAGE': 'http://slimages.macys.com/is/image/MCY/2185465', 'CAT_ID': '7', 'PRODUCT_PRICE': '$560.00', 'PRODUCT_DESC': 'TAHARI ASL BEADED-TRIM SKIRT SUIT NAVYCLOUD 10', 'TYPE': 'PERSONAL' }
               //]

               var suggestedList = JSON.parse(localStorage.getItem("suggestions"))

               if (suggestedList == [] || suggestedList == null) {

                   console.error('No value for suggestion-list')
                   console.log(suggestedList)
               }

               //CHECK EACH ITEM IN SUGGESTED LIST ARE NOT IN WISH LIST 
               console.log('validing suggested products with wishlist')

               //DELETE EXISTING WISHLIST BY FORCE
               // window.localStorage.removeItem('wishlist')
               //alert(localStorage.getItem("wishlist"))

               if (localStorage.getItem("wishlist") != null) {

                   var wishlist = JSON.parse(localStorage.getItem("wishlist"))

                   suggestedList = JSON.stringify(suggestedList)
                   suggestedList = JSON.parse(suggestedList)


                   console.log(wishlist)

                   //REMOVE THE ITEMS EXIST IN WISH LIST OUT OF SUGGESTED LIST
                   for (var wItem in wishlist) {

                       for (var pItem in suggestedList) {

                           if (angular.equals(suggestedList[pItem], wishlist[wItem])) {

                               console.log('Matching found')
                               var index = suggestedList.splice(pItem, 1)
                               continue
                           }

                       }


                   }
               } else { console.log('Wish list is empty') }

               $scope.recommendedProducts = []
               $scope.recommendedProducts = suggestedList
               $scope.$apply()

           }

           /**
            *CHECK ATTRIBUTE
            * @param {string} value
            * @param {string} fallback
            * @export
            */
           function checkAttribute(value, fallback) {
               if (value !== undefined && value !== null && value.toString()) {
                   return value;
               }
               else {

                   return fallback;
               }
           }

           /**
            *ITERATE STRING AND FETCH TO LIST
            * @param {List} dict
            * @param {string} input
            * @param {string} input
            * @export
            */
           function iterateQueryString(dict, input, queryString) {
               console.log(input)
               if (input !== undefined && input !== null && input.length > 0) {
                   for (var i = 0; i < input.length - 1; i++) {
                       if (input[i]['ID'] !== undefined && input[i]['ID'] !== null && input[i]['ID'] > 0) {
                           console.log('Item Add : ' + input[i]['ID'])
                           dataInput[queryString] = input[i]['ID'];
                       }
                   }
               }
           }

           function getProductIDArray(locatStorageDict) {
               var tempArray = []
               if (locatStorageDict != null) {
                   for (var i = 0; i < locatStorageDict.length; i++) {
                       if (locatStorageDict[i]['ID'] != null) {
                           tempArray.push(locatStorageDict[i]['ID']);
                       }
                   }
               }
               return tempArray
           }

           /**
            *LOAD SUGGESTED LIST FROM SERVER AND SAVE IT LOCALLY AND DISPLAY
            * @export
            */
           $scope.saveSuggestionListLocally = function () {
               //wishlist
               //localStorage.getItem("suggestions")
               //localStorage.getItem("selectedCategories")
               //localStorage.getItem("cart")
               //'password': window.localStorage.getItem('password'),

               console.log('Saving suggestions list locally...')
               var userId = window.localStorage.getItem('userId')

               if (userId == '' || userId == null) {
                   console.error('can\'t update local suggestion list NO USERID!!!')
                   return
               }
               console.log('sending suggestion list request count=5')
               var count = 5
               console.log('USER_ID' + userId)
               console.log('COUNT' + count)
               console.log(JSON.parse(localStorage.getItem("wishlist")))
               console.log(JSON.parse(localStorage.getItem("suggestions")))
               console.log(JSON.parse(localStorage.getItem("selectedCategories")))
               
               console.log(JSON.parse(localStorage.getItem("cart")))

               //var dataInput = { 'requestedSuggestionCount': count, 'userID': userId, 'password': window.localStorage.getItem('password') }
               //iterateQueryString(dataInput, localStorage.getItem("wishlist"), 'currentWishlistItemID')
               //iterateQueryString(dataInput, localStorage.getItem("suggestions"), 'currentSuggestionItemID')
               //iterateQueryString(dataInput, localStorage.getItem("selectedCategories"), 'requestedCategoryID')
               //iterateQueryString(dataInput, localStorage.getItem("cart"), 'currentCartItemID')

               var wishlistItems =  getProductIDArray(JSON.parse(localStorage.getItem("wishlist")))
               var suggestionsItems = getProductIDArray(JSON.parse(localStorage.getItem("suggestions")))
               var selectedCategoriesItems =  getProductIDArray(JSON.parse(localStorage.getItem("selectedCategories")))
               var cartItems = getProductIDArray(JSON.parse(localStorage.getItem("cart")))
               console.log("Wishlist")
               console.log(wishlistItems)
               console.log("suggestionsItems")
               console.log(suggestionsItems)
               console.log(selectedCategoriesItems)
               console.log(cartItems)

               $http({
                   url: 'https://research-thushan.c9users.io:8080/api/suggestions',
                   method: "GET",
                   params:
                   {
                       'count': count, 'id': userId, 'currentWishlistItemID': wishlistItems,
                       'currentSuggestionItemID': suggestionsItems,
                       'requestedCategoryID': selectedCategoriesItems,
                       'currentCartItemID': cartItems
                   },
                   //params: dataInput,
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function (response) {

                   var tempArray = []
                   if (response.data.status == 'TRUE') {

                       console.log('saving list in local')
                       console.log(response.data.suggestion_list)
                       tempArray = response.data.suggestion_list

                       //SET LIKE STSTUS
                       for (var item in tempArray) {

                           //SET TYPE
                           tempArray[item].TYPE = 'SOCIAL'

                           if (tempArray[item].USERLIKES == '0') {

                               tempArray[item].ICOLOR = ''
                               tempArray[item].style = ''
                           }
                           else {

                               tempArray[item].ICOLOR = 'red'
                               tempArray[item].style = { "color": "red" }

                           }
                           //CHECK AND SET DEFAULT PRODUCT IMAGE
                           if (tempArray[item].PRODUCT_IMAGE.length == 0 || tempArray[item].PRODUCT_IMAGE == null) {
                               tempArray[item].PRODUCT_IMAGE = 'images/products/blank_product.png'
                           }

                       }


                       localStorage.setItem("suggestions", JSON.stringify(tempArray));
                       $scope.showSuggestedProductList()

                   } else { console.error('no products were suggested!!! ') }

               })


           }

           /**
            *CHECK ATTRIBUTE
            * @param {string} value
            * @param {string} fallback
            * @export
            */
           function checkAttribute(value, fallback) {
               if (value !== undefined && value !== null && value.toString()) {
                   return value;
               }
               else {

                   return fallback;
               }
           }

           /**
            *UPDATE SUGGESTION HANDLER
            * @param {string} status
            * @param {Object} product
            * @param {string} type
            * @param {string} bascket
            * @export
            */
           $scope.updateSuggestionHandler = function (status, product, type, bascket) {

               var userId = window.localStorage.getItem('userId')
               var wishlist = localStorage.getItem("wishlist")

               if (userId == null || userId == '' || userId == '-1') { return }


               var typeName = 'REMOVE';
               if (type) {
                   typeName = 'ADD'
               } else {
                   typeName = 'REMOVE'
               }

               $http({
                   url: 'https://research-thushan.c9users.io:8080/prs/update',
                   method: "GET",
                   params: {
                       'status': status,
                       'userID': userId,
                       'password': window.localStorage.getItem('password'),
                       'productID': checkAttribute(product.ID, 1),
                       'categoryID': checkAttribute(product.categoryID, 1),
                       'suggestionID': checkAttribute(product.suggestionID, 0),
                       'score': checkAttribute(product.score, 0),
                       'weight': checkAttribute(product.weight, 0),
                       'strength': checkAttribute(product.strength, 0),
                       //'status': checkAttribute(product.status, 'TRUE'),
                       'type': checkAttribute(typeName, 'REMOVE'),
                       'modelType': checkAttribute(product.modelType, 'REDUCED'),
                       'bascketType': checkAttribute(bascket, 'REDUCED'),
                       'suggestionType': checkAttribute(product.suggestionType, 'REDUCED')
                   },
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function (response) {

               })
                 $http({
                     url: 'https://research-thushan.c9users.io:8080/api/updateSuggestions',
                   method: "GET",
                   params: {
                       'status': status,
                       'userId': userId

                   },
                   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
               }).then(function (response) {

               })
           }

           /**
            *SEARCH PRODUCT METHOD 
            * @param {string} searchKey
            * @export
            */
           $scope.searchProduct = function (searchKey) {

               console.log('searching on' + searchKey)
               $scope.searchNotice = ''
               var userId = window.localStorage.getItem('userId')
               var catId = $scope.data.category

               if (userId == '' || userId == null) {
                   console.error('can\'t search no user id found !!!')
                   return
               }
               if (catId == null) {

                   console.error('Invalid catId')
                   return
               }

               console.log('Sending http request')
               //MAKE HTTP GET REQUEST FOR SEARCH
               $http({


                   url: 'https://research-thushan.c9users.io:8080/api/fetchSearch',
                   method: "GET",
                   params: { 'queryText': searchKey, 'userId': userId, 'catId': catId }

               }).then(function (response) {

                   var products = response.data
                   console.log('Response arrived fetching data')
                   if (products == '-1') {

                       console.log('Invalid request')
                       $scope.searchNotice = 'No matching results found'
                   }
                   else {

                       var count = 0
                       var tempProductArray = []
                       //ADD COLOR AND STYLE
                       for (var item in products) {

                           count = item
                           if (count >= 30) { break }

                           console.log(products[item])
                           if (products[item].USERLIKES == '0') {
                               products[item].ICOLOR = ''
                               products[item].style = ''
                           } else {
                               products[item].ICOLOR = 'red'
                               products[item].style = { "color": "red" }

                           }
                           //CHECK AND SET DEFAULT PRODUCT IMAGE
                           if (products[item].PRODUCT_IMAGE.length == 0 || products[item].PRODUCT_IMAGE == null) {
                               products[item].PRODUCT_IMAGE = 'images/products/blank_product.png'
                           }

                           tempProductArray.push(products[item])
                           console.log(products[item])


                       }
                       //SAVE SEARCH LIST LOCALLY


                       $scope.searchedProducts = tempProductArray
                       $scope.searchNotice = count + " items found!"
                       $scope.$apply()



                   }


               })

           }

           $scope.saveSuggestionListLocally()
           //$scope.showSuggestedProductList()

           /**
            *SAVE THE PRODUCT IN WISHLIST
            * @param {Object} productObj
            * @export
            */
           $scope.saveInWishList = function (productObj) {
               $scope.updateSuggestionHandler('FALSE', productObj, true, 'WISHLIST')
               var wishList = []
               try {
                   console.log('storing product in the wishlist')
                   console.log(productObj)


                   if (JSON.parse(localStorage.getItem("wishlist")) != null && JSON.parse(localStorage.getItem("wishlist")) != []) { //IF WISH LIST IS NULL

                       var wishList = JSON.parse(localStorage.getItem("wishlist"))

                       var count = 0
                       for (var item in wishlist) {
                           if (angular.equals(productObj, wishlist[item]))
                               count++
                       }

                       console.log(count)
                       if (count == 0)
                           wishList.push(productObj)

                       console.log('Save in local storage')
                       localStorage.setItem("wishlist", JSON.stringify(wishList));
                   }
                   else {
                       var wishlist = []
                       wishList.push(productObj)
                       console.log('Save in local storage')
                       localStorage.setItem("wishlist", JSON.stringify(wishList));
                   }
               } catch (error) {
                   console.error('Error occured while saving item in wishlist')
                   console.error(error)

                   $ionicPopup.alert({
                       title: 'Oops!',
                       template: 'Please try again '
                   })

               }
               finally {

                   $ionicPopup.alert({
                       title: 'Saved',
                       template: 'Your product is saved in wishlist. Please check your wishlist to purchase it '
                   })

                   //REMOVE THE PRODUCT FROM SUGGESTED LIST 
                   var index = $scope.recommendedProducts.indexOf(productObj)
                   console.log(productObj)
                   if (index != '-1') {
                       $scope.recommendedProducts.splice(index, 1)
                       $scope.$apply()
                   }
                   var index = $scope.searchedProducts.indexOf(productObj)
                   if (index != '-1') {

                       $scope.searchedProducts.splice(index, 1)
                       $scope.$apply()
                   }


                   //IF SELECTED ITEM IS IN SEARCH REMOVE IT

                   //if ($scope.searchedProducts != [] && $scope.searchedProducts != null) {

                   //    for (var item in $scope.searchedProducts) {
                   //        if (angular.equals($scope.searchedProducts[item], productObj)) {
                   //              var index = $scope.searchedProducts.indexOf(productObj)
                   //               $scope.recommendedProducts.splice(index, 1)
                   //              $scope.$apply()
                   //        }
                   //    }

                   //}

               }



           }

           /**
            *SAVE THE PRODUCT IN WISHLIST
            * @param {string} productId
            * @param {int} amount
            * @export
            */
           $scope.showLikesList = function (productId, amount) {

               var firstname = window.localStorage.getItem('firstname')

               //get the like list of users for this productId  and build the below popUp window
               var likeCount = 0
               var templ = ''

               $http({


                   url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/getlikes',
                   method: "GET",
                   params: { pid: productId }

               }).then(function (response) {

                   var result = response.data

                   var item = 0


                   for (item in result) {

                       var image = result[item].image
                       var username = result[item].userName

                       if (firstname == username) {
                           username = 'You'
                       }


                       templ = templ + '<div class="list"><a class="item item-avatar" href="#"><img src="' + image + '"><h4>' + username + '</h4><p>like this</p></a></div>'
                       likeCount = likeCount + 1


                   }


                   $ionicPopup.alert({
                       title: likeCount + ' people like this',
                       template: templ
                   });



               })








           }

           /**
            *SAVE THE PRODUCT IN WISHLIST
            * @param {string} productId
            * @export
            */
           $scope.showComment = function (productId) {

               //get the comments list of users for this productId  and build the below popUp window 
               //also get the no of comments var commentCount

               var addCommentTmp = '<div class="list"> <label class="item item-input item-floating-label"><span class="input-label">Your comment</span><input type="text" ng-model="data.com" placeholder="type your comment" ng-focus="A()"></label></div>'
               var userId = window.localStorage.getItem('userId')
               var firstname = window.localStorage.getItem('firstname')

               $http({


                   url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/getcomments',
                   method: "GET",
                   params: { pid: productId }

               }).then(function (response) {
                   console.log(" server responded")
                   var result = response.data
                   var temp = addCommentTmp
                   var item = 0
                   var commentCount = 0

                   for (var item in result) {

                       var image = result[item].image
                       var username = result[item].userName
                       var uid = result[item].userId
                       var comment = result[item].status
                       var cid = result[item].id

                       if (uid == userId) {
                           //temp = temp + '<div class="list"><a class="item item-avatar" href="#"><img src=' + image + '><h4>' + username + '</h4><p>' + comment + '</p><p><ion-option-button class="button-light button-small icon ion-edit"></ion-option-button><ion-option-button class="button-assertive button-small icon ion-trash-a"></ion-option-button></p></a></div>'
                           temp = temp + '<div class="list"><a class="item item-avatar " href="#"><img src=' + image + '><h4>' + username + '</h4><p>' + comment + '</p><div class="buttons"><button class="button button-small button-stable" ng-click="editComment(' + cid + ')"><i class="icon ion-edit"></i> Edit </button><button class="button button-small button-assertive"><i class="icon ion-trash-a"></i>Delete</button></div></a></div>'
                           commentCount = commentCount + 1

                       }

                       else {
                           temp = temp + '<div class="list"><a class="item item-avatar" href="#"><img src=' + image + '><h3>' + username + '</h3><p>' + comment + '</p></a></div>'
                           commentCount = commentCount + 1
                       }


                   }

                   //temp = temp + addCommentTmp
                   console.log(temp)
                   $ionicPopup.show({


                       title: commentCount + " comments on this product",
                       subTitle: 'Add your comment below',
                       template: temp,
                       scope: $scope,
                       buttons: [
                         { text: 'Close' },
                         {
                             text: 'Add',
                             type: 'button-positive',
                             onTap: function (e) {

                                 if (!$scope.data.com) {

                                     e.preventDefault()
                                 }
                                 else {

                                     $http({


                                         url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/addcomments',
                                         method: "POST",
                                         params: { pid: productId, uid: userId, uname: firstname, image: 'images/avatar.png', comment: $scope.data.com },
                                         headers: { 'Content-Type': 'text/html' }

                                     }).then(function (response) {

                                         var result = response.data
                                         var res = parseInt(result.toString(), 10)

                                         if (res > 0) {
                                             $scope.data.com = ''


                                         }


                                     })

                                 }




                             }
                         }]



                   })

                   $scope.editComment = function (commentId) {
                       console.log(commentId)

                       $http({


                           url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/getcommentbyid',
                           method: "GET",
                           params: { cid: commentId }

                       }).then(function (response) {

                           var result = response.data
                           var item = 0
                           var comment = ''

                           for (item in result) {

                               comment = result[item].status

                           }
                           console.log(comment)

                           $http({
                               url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/removecomments',
                               method: 'DELETE',
                               params: { cid: commentId }
                           }).then(function (response) {

                               var result = response.data
                               var res = parseInt(result.toString(), 10)


                               if (res > 0) {
                                   $scope.data.com = comment

                               }

                           })
                       })

                   }

               })

           }

           /**
            *Add like
            * @param {string} upc
            * @param {string} products
            * @param {string} scene
            * @export
            */
           $scope.addLike = function (upc, products, scene) {
               //get the like list of users for this productId  and build the below popUp window

               var firstname = window.localStorage.getItem('firstname')
               var userId = window.localStorage.getItem('userId')
               var image = window.localStorage.getItem('image')

               var status = ''

               var index = -1
               var selectedProduct = []
               for (var i in products) {
                   if (products[i].UPC == upc) {

                       selectedProduct = products[i]
                       if (scene == 'suggest')
                           $scope.recommendedProducts.splice(i, 1)
                       if (scene == 'search')
                           $scope.searchedProducts.splice(i, 1)
                       $scope.$apply()
                       index = i
                   }
               }
               console.log('UPC ' + upc)
               console.log('PRODUCT ' + products)
               console.log('index ' + index)

               $http({


                   url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/checklikes',
                   method: "GET",
                   params: { pid: upc, uid: userId }

               }).then(function (response) {

                   var result = response.data
                   var item = 0

                   for (item in result) {

                       status = result[item].status

                   }

                   if (parseInt(status, 10) == 1) {


                       $http({
                           url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/removelikes',
                           method: 'DELETE',
                           params: { 'pid': upc, 'uid': userId }
                       }).then(function (response) {

                           var result = response.data
                           var res = parseInt(result.toString(), 10)

                           if (res > 0) {



                               //UPDATE LIKE COLOR
                               selectedProduct.ICOLOR = ''
                               selectedProduct.style = { "color": "" }

                               //UPDATE LIKE COUNTER
                               var likes = selectedProduct.LIKES
                               likes--
                               selectedProduct.LIKES = likes

                               if (scene == 'suggest') {
                                   $scope.recommendedProducts.splice(index, 0, selectedProduct)
                                   $scope.recommendedProducts.join()

                                   //UPDATE SAME PRODUCT IN RECOMMEND LIST IF EXISTS
                                   for (var search in $scope.searchedProducts) {
                                       if (angular.equals($scope.searchedProducts[search], selectedProduct)) {

                                           console.log('item found in search result.remove like')
                                           $scope.searchedProducts[search] = selectedProduct
                                       }

                                   }

                               }

                               if (scene == 'search') {
                                   $scope.searchedProducts.splice(index, 0, selectedProduct)
                                   $scope.searchedProducts.join()


                                   //UPDATE SAME PRODUCT IN SEARCHED RESULT LIST IF EXISTS
                                   for (var search in $scope.recommendedProducts) {
                                       if (angular.equals($scope.recommendedProducts[search], selectedProduct)) {

                                           console.log('item found in suggested result.remove like')
                                           $scope.recommendedProducts[search] = selectedProduct
                                       }

                                   }

                               }
                               $scope.$apply()


                           }

                       })
                   }
                   else {

                       $http({

                           url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/addlikes',
                           method: "POST",
                           params: { 'pid': upc, 'uid': userId, 'uname': firstname, 'image': image, 'status': 1 },
                           headers: { 'Content-Type': 'text/html' }

                       }).then(function (response) {

                           var result = response.data
                           var res = parseInt(result.toString(), 10)

                           if (res > 0) {

                               //UPDATE COLOR
                               selectedProduct.ICOLOR = 'red'
                               selectedProduct.style = { "color": "red" }


                               //UPDATE LIKE COUNTER
                               var likes = selectedProduct.LIKES
                               likes++
                               selectedProduct.LIKES = likes

                               if (scene == 'suggest') {
                                   $scope.recommendedProducts.splice(index, 0, selectedProduct)
                                   $scope.recommendedProducts.join()

                                   //UPDATE SAME PRODUCT IN SEARCHED RESULT LIST IF EXISTS
                                   for (var search in $scope.searchedProducts) {
                                       if (angular.equals($scope.searchedProducts[search], selectedProduct)) {

                                           console.log('item found in search result.add like')
                                           $scope.searchedProducts[search] = selectedProduct
                                       }

                                   }

                               }

                               if (scene == 'search') {

                                   $scope.searchedProducts.splice(index, 0, selectedProduct)
                                   $scope.searchedProducts.join()


                                   //UPDATE SAME PRODUCT IN SEARCHED RESULT LIST IF EXISTS
                                   for (var search in $scope.recommendedProducts) {
                                       if (angular.equals($scope.recommendedProducts[search], selectedProduct)) {

                                           console.log('item found in suggested result.add like')
                                           $scope.recommendedProducts[search] = selectedProduct
                                       }

                                   }
                               }


                               $scope.$apply()

                           }


                       })
                   }

               })


           }

           /**
            *Delete item from Recieved-Friend-Requests list
            * @param {Object} item
            * @export
            */
           $scope.deleteFriendRequest = function (item) {

               console.log('removing item from RecievedChatRequests index-of ' + item)
               var index = $scope.FriendshipRequests.indexOf(item);
               $scope.FriendshipRequests.splice(index, 1);
               // $scope.RecievedChatRequests.splice($scope.RecievedChatRequests.indexOf(item),1)
           }

           /**
            *Delete item from Recieved-Chat-Requests list
            * @param {Object} item
            * @export
            */
           $scope.deleteChatRequest = function (item) {

               console.log('removing item from RecievedChatRequests index-of ' + item)
               var index = $scope.RecievedChatRequests.indexOf(item);
               $scope.RecievedChatRequests.splice(index, 1);
               // $scope.RecievedChatRequests.splice($scope.RecievedChatRequests.indexOf(item),1)
           }

           /**
           *navigate product popularity view
           * @param {string} productId
           * @export
           */
           $scope.loadProductInfo = function (productId) {

               if (productId > 0) {

                   console.log('displaying product info for product ' + productId)
                   window.localStorage.removeItem('productId')
                   //$cookieStore.put('productId', productId)
                   window.localStorage.setItem('productId', productId)
                   window.location = '#/app/popularity'

               }


           }

           /**
           * show chat friends list view for chosen product
           * @param {string} productCatId
           * @param {Object} productObj
           * @export
           */
           $scope.displayChatFriendsView = function (productCatId, productObj) {

               //save product object and UPC Id
               window.localStorage.setItem('product', JSON.stringify(productObj))
               window.localStorage.setItem('productId', productObj.UPC)


               //save the product category id as in $cookieStore
               // $cookieStore.remove('catId');
               window.localStorage.removeItem('catId')
               // $cookieStore.put('catId', productCatId)
               window.localStorage.setItem('catId', productCatId)

               //navigate to suggest friends for chat view

               //check user is valid and category id is valid
               var tempCatId = window.localStorage.getItem('catId')//$cookieStore.get('catId')
               var tempUserId = window.localStorage.getItem('userId') //$cookieStore.get('userId')

               if (tempUserId != '-1' || tempUserId != '0' || tempUserId != '' && (9 > tempCatId > 0)) { //valid

                   //navigate
                   window.location = '#/app/askfriend';
                   console.log('cat-id' + tempCatId + ' uid' + tempUserId)

               }
               else {
                   console.log('error in user id or cat id')
                   console.log('cat id' + tempCatId)
                   console.log('user id' + tempUserId)

               }
           }

           //navigateCommentView
           $scope.navigateCommentView = function (productId) {
               //save product id
               window.localStorage.setItem('productId', productId)
               window.location = '#/app/commentview'
           }

           // tab-2 timeline N/A
           // tab-3 my friends

           /**
           * show chat friends list view for chosen product
           * @param {string} searchKey
           * @export
           */
           $scope.resetUserSearch = function (searchKey) {
               if (searchKey == '' || searchKey == null) {
                   //alert('resetting')

                   $scope.suggestedFriendList = []
                   $scope.existingFriendList = []
                   //$scope.errorNewFriendsMsg=null
                   $scope.loadFriends()
                   $scope.loadFriendRequests()

               }

           }

           /**
           * replace blank  profile image with default image
           * @param {string} image
           * @export
           */
           $scope.resolveProfileImage = function (image) {

               if (image == null) {

                   image = 'images/avatar.png'
                   return image
               }
               if (image.toString() == 'None') {
                   console.log('Profile pic replaced with default')
                   image = 'images/avatar.png'
                   return image
               } else {
                   return image
               }


           }

           /**
           * set relationship type 
           * @param {Obhect} item
           * @export
           */
           $scope.fixRelationshipType = function (item) {

               if (item == 'friend') {
                   item = 'A friend of yours'

               }
               else if (item == 'family') {

                   item = 'A family member'
               }
               else {
                   item = 'Unkown relationship'
               }
               return item
           }

           /**
            * loading suggested-new-friends 
            * @param {Obhect} item
            * @export
            */
           $scope.loadFriends = function () {

               $scope.existingFriendList = []
               $scope.suggestedFriendList = []
               $scope.searchUsersTitle = ' Loading content. please wait...'
               $scope.SuggestedFriends = ''
               $scope.data.badge = null
               var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')
               //alert('loading friends of ' + userId)
               console.log('loading friends of ' + userId)

               if (userId != '-1' || userId != '0') {
                   // make http request for existing friend list

                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/allfriends/',
                       method: "GET",
                       params: { 'userId': userId }
                   }).then(function (response) {


                       var values = response.data
                       var friends = []
                       var value = ''
                       var error = ''
                       var errMsg = ''
                       var count = 0
                       //alert(values)

                       for (value in values) {
                           if (values[value].firstname != 'Sorry') {

                               //enhance relationship  type
                               values[value].rel_type = $scope.fixRelationshipType(values[value].rel_type)
                               //set default profile picture if given url is empty or null
                               values[value].image = $scope.resolveProfileImage(values[value].image)

                               friends[value] = values[value]
                               count++
                           }
                           else {
                               error = values[value].firstname
                               errMsg = values[value].email
                               count = -1
                               break


                           }
                       }

                       if (count != -1) {
                           $scope.existingFriendList = friends
                           $scope.searchUsersTitle = ''
                       }
                       else {
                           $scope.searchUsersTitle = 'Sorry! nothing to show '
                           //$scope.errorNewFriendsMsg = error + "," + errMsg
                           //alert('' + errMsg)
                       }

                   })


                   // make request for new friends list
                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/newfriends/',
                       method: "GET",
                       params: { userId: userId }
                   }).then(function (response) {


                       var values = response.data
                       var friends = []
                       var value = ''
                       var error = ''
                       var errMsg = ''
                       var count = 0

                       for (value in values) {

                           //console.log(values[value])

                           values[value].image = $scope.resolveProfileImage(values[value].image)


                           friends[value] = values[value]
                       }


                       $scope.suggestedFriendList = friends

                   })




               }
               else { console.log('error in sending request. Invalid user id') }


           }

           /**
            * removes existing friendship 
            * @param {Obhect} candidate
            * @export
            */
           $scope.breakFriendship = function (candidate) {
               $ionicPopup.show({

                   title: " Unfriend " + candidate.firstname,
                   subTitle: 'Are you sure to delete this relationship?',
                   scope: $scope,
                   buttons: [
                  { text: 'Cancel' },
                  {
                      text: '<b>Unfriend</b>',
                      type: 'button-positive',
                      onTap: function (e) {

                          // alert("selected: " + $scope.data.relsts + "; fid: " + suggestedFriend.userId);
                          var friendId = candidate.userId
                          var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')

                          if (userId != '-1' && userId != '0' && friendId != '-1' && friendId != '0' && friendId != '' && userId != '') //validate user ids

                              //send requests
                              $http({ url: "https://research-thushan.c9users.io:8080/beforecart/frs/unfriend/", method: 'GET', params: { userId: userId, friendId: friendId } }).then(function (response) {
                                  var response = response.data

                                  if (response == '1') {
                                      $ionicPopup.alert({
                                          title: 'Friendship deleted',
                                          template: 'You have removed ' + candidate.firstname + ' from friends list'
                                      });
                                      //refresh and reload model
                                      // $scope.onItemDelete(candidate)
                                      $scope.existingFriendList = null

                                      $scope.suggestedFriendList = null
                                      //$scope.errorNewFriendsMsg = null
                                      $scope.SuggestedFriends = ''
                                      $scope.loadFriends()
                                      $scope.loadFriendRequests()
                                  }
                                  else if (response != '1') {
                                      $ionicPopup.alert({
                                          title: 'Ooops',
                                          template: 'We were unable to remove this relationship. Please try again'
                                      });
                                  }
                              })


                      }
                  }
                   ]
               });



           }

           /**
           * send friend request 
           * @param {Object} candidate
           * @export
           */
           $scope.sendFriendRequest = function (suggestedFriend) {

               $ionicPopup.show({

                   title: "Send request to " + suggestedFriend.firstname,
                   subTitle: 'Are you sure to  send this request?',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Send</b>',
                         type: 'button-positive',
                         onTap: function (e) {

                             // alert("selected: " + $scope.data.relsts + "; fid: " + suggestedFriend.userId);
                             var friendId = suggestedFriend.userId
                             var userId = window.localStorage.getItem('userId')// $cookieStore.get('userId')

                             if (userId != '-1' && userId != '0' && friendId != '-1' && friendId != '0' && friendId != '' && userId != '') //validate user ids
                                 //send requests
                                 $http({ url: "https://research-thushan.c9users.io:8080/beforecart/frs/sendrequest/", method: 'GET', params: { 'userId': userId, 'friendId': friendId } }).then(function (response) {
                                     var response = response.data

                                     if (response == '200') {
                                         $ionicPopup.alert({
                                             title: 'Request is Sent',
                                             template: 'Your request has successfully made'
                                         });
                                         //refreshing ui
                                         $scope.suggestedFriendList = null
                                         $scope.existingFriendList = null
                                         //$scope.errorNewFriendsMsg = null
                                         $scope.onItemDelete(suggestedFriend)
                                         $scope.loadFriends()
                                         $scope.loadFriendRequests()
                                     }
                                     else {
                                         $ionicPopup.alert({
                                             title: 'Ooops',
                                             template: 'Your request could not be saved'
                                         });
                                     }
                                 })


                         }
                     }
                   ]
               });

           }

           /*
               tab-4 notices
           */

           /**
           * Display friend requests
           * @param {list} requests
           * @export
           */
           socket.on('propChat', function (requests) {

               var userId = window.localStorage.getItem('userId')
               var tempArr = []
               for (var item in requests) {
                   if (requests[item].to == userId) { //check message  reciever
                       tempArr.push(requests[item])
                   }

               }
               $scope.RecievedChatRequests = tempArr
               $scope.$apply()
               if (tempArr.length != 0) { //prepare badge

                   $scope.data.badge = 'New'
               }
               else {
                   $scope.data.badge = null
               }

           })

           /**
           * laod and display all friend request from server
           * @param {list} requests
           * @export
           */
           $scope.loadFriendRequests = function () {
               var userId = window.localStorage.getItem('userId')//$cookieStore.get('userId')

               $scope.FriendRequestTitle = 'Sorry! nothing to show'
               if (userId != '0' && userId != '-1') {

                   //send request to get all 
                   $http({ url: "https://research-thushan.c9users.io:8080/beforecart/frs/recievedrequests/", method: 'GET', params: { userId: userId } }).then(function (response) {

                       // fetch response
                       var results = response.data
                       for (var result in results) {
                           console.log(results[result].userId)
                           if (results[result].userId == '-1') { // no errors in response or response is not empty
                               $scope.FriendRequestTitle = 'Sorry! nothing to show '
                               $scope.data.badge = null
                               $scope.FriendshipRequests = []
                               return

                           }
                           else {
                               results[result].image = $scope.resolveProfileImage(results[result].image)

                               $scope.FriendRequestTitle = ''
                               $scope.data.badge = 'New'
                               $scope.FriendshipRequests = results
                               return
                           }

                       }




                   }) //end of loading friendship requests
               }

           }

           /**
           *reject a recieved friend request
           * @param {Object} FriendshipRequest
           * @export
           */
           $scope.rejectFriendshipRequest = function (FriendshipRequest) {

               $ionicPopup.show({

                   title: "Delete Request ",
                   subTitle: '<p>Are you sure to remove the friendship request?</p>',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Reject</b>',
                         type: 'button-positive',
                         onTap: function (e) {
                             var friendId = FriendshipRequest.userId //get params
                             var userId = window.localStorage.getItem('userId') // $cookieStore.get('userId')


                             if (friendId != '') {

                                 try {
                                     //send reject request request
                                     $http({ url: 'https://research-thushan.c9users.io:8080/beforecart/frs/rejectrequest/', method: 'GET', params: { userId: userId, friendId: friendId } }).then(function (response) {

                                         if (response.data == '200') //successfully  rejected
                                         {
                                             $ionicPopup.alert({
                                                 title: 'Request Rejected',
                                                 template: 'The request has been cancelled'
                                             });
                                             //refreshing ui
                                             //$scope.suggestedFriendList = null
                                             //$scope.existingFriendList = null
                                             //$scope.errorNewFriendsMsg = null
                                             //$scope.suggestedFriendList.push(FriendshipRequest)
                                             $scope.suggestedFriendList.push(FriendshipRequest)
                                             $scope.deleteFriendRequest(FriendshipRequest)

                                             // $scope.FriendshipRequests = null//refresh ui
                                             $scope.data.badge = null
                                             $scope.$apply();
                                             //$scope.loadFriendRequests() 
                                             //$scope.loadFriends()



                                         }
                                         else {

                                             $ionicPopup.alert({
                                                 title: 'Oops',
                                                 template: 'Could not reject request.Something went wrong.Please try again'
                                             });

                                         }


                                     })
                                 }
                                 catch (err) {
                                     alert(err.message)
                                 }


                             }

                         }
                     }]
               })






           }

           /**
           *accept friend request
           * @param {Object} FriendshipRequest
           * @export
           */
           $scope.acceptFriendshipRequest = function (FriendshipRequest) {

               var response = true;
               $ionicPopup.show({

                   title: "Confirm friendship ",
                   subTitle: '<p>I know ' + FriendshipRequest.firstname + " because he / she is a ",
                   template: '<div class="list">  <div class="item item-input item-select"><div class="input-label">Pick One<div><select ng-model="data.reltype"><option value="friend" selected>Friend of mine </option><option value="family">Family member</option></select></div> </div>',
                   scope: $scope,
                   buttons: [
                     { text: 'Cancel' },
                     {
                         text: '<b>Accept</b>',
                         type: 'button-positive',
                         onTap: function (e) {

                             if (!$scope.data.reltype) {
                                 e.preventDefault()
                             }
                             else {

                                 //send request
                                 console.log('sending accept friendship request...')
                                 var friendId = FriendshipRequest.userId //get params
                                 var userId = window.localStorage.getItem('userId') // $cookieStore.get('userId')

                                 if (friendId != '') {

                                     try {
                                         //send accept request
                                         $http({ url: 'https://research-thushan.c9users.io:8080/beforecart/frs/acceptrequest/', method: 'GET', params: { userId: userId, friendId: friendId, type: $scope.data.reltype } }).then(function (response) {

                                             if (response.data == '200') //successfully  accepted
                                             {
                                                 $scope.deleteFriendRequest(FriendshipRequest)
                                                 $scope.existingFriendList = []
                                                 //$scope.loadFriends()
                                                 $scope.existingFriendList.push(FriendshipRequest)

                                                 //$scope.$apply()

                                                 $ionicPopup.alert({
                                                     title: 'Friendship created',
                                                     template: 'You and ' + FriendshipRequest.firstname + " are now friends"
                                                 });
                                                 console.log('request accepted. refreshing model...')
                                                 //refreshing ui

                                                 //$scope.suggestedFriendList = []
                                                 //$scope.existingFriendList = []
                                                 //$scope.errorNewFriendsMsg = null
                                                 console.log(FriendshipRequest)
                                                 //$scope.onItemDelete(FriendshipRequest)

                                                 //$scope.FriendshipRequests = []//refresh ui
                                                 $scope.data.badge = null
                                                 response = true


                                             }
                                             else {
                                                 response = false
                                                 $ionicPopup.alert({
                                                     title: 'Oops',
                                                     template: 'Could not reject request.Something went wrong.Please try again'
                                                 });

                                             }


                                         })
                                     }
                                     catch (err) {
                                         console.log('error in accepting friend request')
                                         console.log(err.message)
                                     }
                                     finally {
                                         FriendshipRequest.chats=0
                                         $scope.existingFriendList.push(FriendshipRequest)
                                         $scope.$apply()

                                     }
                                 }



                             }

                         }
                     }]
               })



           }

           /**
           *delete item from list
           * @param {Object} item
           * @export
           */
           $scope.onItemDelete = function (item) {
               $scope.items.splice($scope.items.indexOf(item), 1);
           }

           /**
           * navigate user to the chat client room
           * @param {Object} ChatRequest
           * @export
           */
           $scope.navigateChatClientRoom = function (ChatRequest) {

               //$cookieStore.put('chatOwner', ChatRequest.userId)
               window.localStorage.setItem('chatOwner', ChatRequest.userId)
               window.location = '#/app/chatclient'
           }

           /**
           * refresh
           * @export
           */
           $scope.refresh = function () {

               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };

           /**
           * remove chat invitation from socket list
           * @param {Object} chatRequest
           * @export
           */
           $scope.removeChatRequestAtSocket = function (chatRequest) {

               console.log('denying chat request.check server console')
               var client = { 'userId': chatRequest.to }
               socket.emit('removeClient', client)
               socket.emit('denyChatRequest', chatRequest)
           }

           /**
           * ignore chat invitation 
           * @param {Object} chatRequest
           * @export
           */
           $scope.ignoreInvitation = function (ChatRequest) {
               $ionicPopup.show({

                   title: "Delete Invitation ",
                   subTitle: '<h5>Do you want to delete this invitation?</h5>',
                   scope: $scope,
                   buttons: [
                  { text: 'No' },
                  {
                      text: '<b>Yes</b>',
                      type: 'button-positive',
                      onTap: function (e) {
                          $scope.removeChatRequestAtSocket(ChatRequest) //remove at socket


                          //remove chat-request item from chat-requests list
                          console.log('removing invitation from local list...')
                          $scope.onItemDelete(ChatRequest)

                          //disconnect the :CHATTING relationship from remote database

                          //aquire user and friend id
                          console.log('aquiring and validating friendId and userId ....')
                          var userId = window.localStorage.getItem('userId') //$cookieStore.get('userId')
                          var friendId = ChatRequest.userId

                          //validate ids 
                          if (userId != null && friendId != null) {

                              console.log('user ids are valid.preparing to delete chat request in db... ')

                              //prepare and send http request 
                              $http({ url: 'https://research-thushan.c9users.io:8080/beforecart/frs/denychatrequest/', method: 'DELETE', params: { userId: userId, friendId: friendId } }).then(function (response) {

                                  //analyze response
                                  console.log('analizing response for denychatrequest... ')
                                  var status = response.data

                                  if (status == '1') { //successfully removed relationship

                                      console.log('any chatting relationship between' + userId + ' and ' + friendId + ' being removed,updating model maincatelog.html ...')

                                      //recall view all chat request method
                                      console.log('loading existing chat requests and friend requests...')
                                      $scope.data.badge = null
                                      $scope.loadFriendRequests()



                                  }
                                  else if (status == '-1') { //removal failed

                                      console.log('somthing wrong at server check server console..')
                                      $scope.data.badge = null

                                  }
                                  else {
                                      console.log('request not proccessed. check server console..')
                                      $scope.data.badge = null
                                  }

                              })

                          }

                      }
                  }]
               })





           }

           /**
           * state changed event
           * @param {Event} event
           * @param {State} toState
           * @param {State} fromState
           * @param {Params} fromParams
           * @export
           */
           $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
               //1937
               if ((toState.name == "app.mainCatelog" && fromState.name == 'app.changeAccount') || (toState.name == "app.mainCatelog" && fromState.name == 'app.stall')) {
                   console.log('FROM:app.mainCatelog loading friends') //|| (toState.name == "app.mainCatelog" && fromState.name == 'app.findfriends')
                   $scope.loadFriends()
                   $scope.loadFriendRequests()                  
                   $scope.showSuggestedProductList()

                   //$scope.loadChatRequests()


               }
           })

       }])

       /**
       *Home controller
       *
       * @param {!angular.Scope} $scope, $state, $http, $ionicPopup, $route, myappService
       * @constructor
       * @ngInject
       * @export
       **/
       .controller("homeCtrl", ["$scope", "$state", "$http", '$ionicPopup', '$route', 'myappService', function ($scope, $state, $http, $ionicPopup, $route, myappService) {

           var email = window.localStorage.getItem('email')
           var firstname = window.localStorage.getItem('firstname')
           var userId = window.localStorage.getItem('userId')
           console.log('check ongoing chat exists')
           var chatOwner = window.localStorage.getItem('chatOwner')
           var chatUsers = window.localStorage.getItem('chatUser')
           if (chatOwner != null && chatUsers == null) {

               //$ionicPopup.alert({
               //    title: 'Sorry',
               //    template: "There is an ongoing chat which you haven't finished yet"
               //});

               window.location = '#/app/chatclient'
           } else if (chatOwner != null && chatUsers != null) {

               window.location = '#/app/askfriend'
           }

           if (email != null && firstname != null && userId != null) {
               window.location = '#/app/mainCatelog'
               console.log('user is still logged in. displaying main categlog...')
           }

           /**
           * check user is online from Server
           * @export
           */
           $scope.checkUserIsOnline = function () {
               var email = window.localStorage.getItem('email')//$cookieStore.get('email')

               if (email != '' || email != null) {

                   $http({

                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/isonline/',
                       method: "GET",
                       params: { email: email }

                   }).then(function (response) {


                       var state = response.data

                       return state


                   });

               }

           }

           /**
           *check user is signed in and controll navigation
           * @param {string} ridection
           * @export
           */
           $scope.naviControl = function (ridection) {
               console.log('executing naviControl ...')

               //aquire chat data
               // var chatData =  $cookieStore.get('chatData')
               var chatUsers = JSON.parse(window.localStorage.getItem('chatUsers')) // $cookieStore.get('chatUsers')
               var chatOwner = window.localStorage.getItem('chatOwner')  //  $cookieStore.get('chatOwner')
               console.log(chatUsers)
               var userId = window.localStorage.getItem('userId')//$cookieStore.get('userId')
               var isOnlineAtServer = $scope.checkUserIsOnline()
               if ((chatUsers == null || chatUsers == '') && (chatOwner == null || chatOwner == '')) {

                   if (userId > 0) { //check user is logged in

                       switch (ridection) {

                           case 'catelog':
                               window.location = '#/app/mainCatelog';
                               break;
                           case 'settings':
                               window.location = '#/app/changeAccount';
                               break;
                           case 'wishlist':
                               window.location = '#/app/stall';
                               break;
                           default:
                               //window.location.reload();
                               window.location = '#/app/home';

                       }



                   }
                       //user not logged in prevent loading above views
                   else {

                       $ionicPopup.show({

                           title: "Sign In ",
                           subTitle: '<p>Your session has expired. Please Sign In to continue</p> ',
                           template: '<div class="list">  <label class="item item-input"> <span class="input-label">Email</span><input type="email" ng-model="data.email"> </label> <label class="item item-input"> <span class="input-label">Password</span> <input type="password" ng-model="data.password"> </label>    </div>',
                           scope: $scope,
                           buttons: [
                             { text: 'Cancel' },
                             {
                                 text: '<b>Sign In</b>',
                                 type: 'button-positive',
                                 onTap: function (e) {
                                     if (!$scope.data.email && !$scope.data.password) {

                                         e.preventDefault()
                                     }
                                     else {
                                         console.log('signing in as uid:' + userId)
                                         $scope.signIn($scope.data.email, $scope.data.password)

                                     }



                                 }
                             }]
                       })
                   }
               } else { // ongoing  chat not concluded
                   console.log('attempted to open side menu while $cookieStore(chatUsers) ==null')

                   $ionicPopup.alert({
                       title: 'Sorry',
                       template: 'Please finish the ongoing chat to continue'

                   });

                   //redirect  back to chat room
                   if (chatUsers != '' && chatOwner == null) //current user is owner of chat session
                       window.location = '#/app/chatView' //'#/app/askfriend'
                   else if (chatOwner != '' && chatUsers == null) //current user is a subscriber for chat session
                       window.location = '#/app/chatclient'
               }



           }

           /**
           * Finish all exsisting chat relationships
           * @export
           */
           $scope.endChatAllEdges = function () {

               var userId = window.localStorage.getItem('userId')
               if (userId != '' && userId != '-1' && userId != '0' && userId != null) { //validate user id
                   // alert('Chat data saved  OK')
                   //make  request
                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/endchat/',
                       method: "GET",
                       params: { userId: userId }
                   }).then(function (response) {

                       if (response.data == '1') {
                           console.log('All chat edges deleted ')
                           window.localStorage.removeItem('chatOwner') //delete chat owner only if all chat edges are deleted in db
                           window.localStorage.removeItem('chatUsers') //remove all chat users
                       }
                       else {
                           console.error('Internal server logic error. See server console')
                       }
                   },
                   function (error) { console.error('HTTP request error endChatSessions() ' + error) });

                   return true
               } else {
                   console.log('No remaining chat session detected to delete')
                   return true
               }
           }

           /**
           * sign out user and clean session
           * @export
           */
           $scope.signout = function () {
               var userId = window.localStorage.getItem('userId')//    $cookieStore.get('userId')
               console.log(userId)
               if (userId != null) {
                   $ionicPopup.show({

                       title: "Sign Out?",
                       subTitle: '<h6>Please note that sign out will clear all of your on going chats and wishlist. Are you sure to sign out?</h> ',
                       scope: $scope,
                       buttons: [
                         { text: 'Cancel' },
                         {
                             text: '<b>Sign out</b>',
                             type: 'button-positive',
                             onTap: function (e) {

                                 //get user identity
                                 var userId = window.localStorage.getItem('userId') // $cookieStore.get('userId')
                                 var userEmail = window.localStorage.getItem('email')// $cookieStore.get('email')


                                 //sending request to sign out user if userid and email are valid
                                 if (userId != null && userEmail != null) {

                                     $http({

                                         url: 'https://research-thushan.c9users.io:8080/beforecart/frs/signout/',
                                         method: "GET",
                                         params: { email: userEmail, userId: userId }

                                     }).then(function (response) {

                                         var status = response.data
                                         //if sign-out success
                                         if (status == 1) {

                                             //window.location.reload()
                                             // $cookieStore.remove('userId');  //remove user identity locally and refresh screen
                                             //  $cookieStore.remove('email');
                                             // $cookieStore.remove('firstname');
                                             //  $cookieStore.remove('chatUsers');
                                             // $cookieStore.remove('chatData');
                                             $scope.endChatAllEdges()
                                             window.localStorage.removeItem('userId')
                                             window.localStorage.removeItem('email')
                                             window.localStorage.removeItem('firstname')
                                             window.localStorage.removeItem('chatUsers')
                                             window.localStorage.removeItem('chatData')
                                             window.localStorage.removeItem('productId')
                                             window.localStorage.removeItem("wishlist")
                                             window.localStorage.removeItem('password')
                                             location.reload()


                                             window.location = '#/app/home'
                                         }
                                         else {

                                             window.localStorage.removeItem('userId')
                                             window.localStorage.removeItem('email')
                                             window.localStorage.removeItem('firstname')
                                             window.localStorage.removeItem('chatUsers')
                                             window.localStorage.removeItem('chatData')
                                             window.localStorage.removeItem("wishlist")
                                             window.localStorage.removeItem('password')
                                             location.reload()
                                             console.error(' error in signout() server returned:' + status + ' check server console')
                                             //$ionicPopup.alert({
                                             //    title: 'Oops',
                                             //    template: 'Before-Cart signed out with errors.Please try again '
                                             //});
                                             window.location = '#/app/home'

                                         }


                                     });
                                 }


                             }
                         }]
                   })


               }



           }

           /**
           * Invoke sign in function
           * @param {String} username
           * @param {String} password
           * @export
           */
           $scope.signIn = function (username, password) {

               // get credentails from models
               var email = username
               var password = password
               $scope.userId = '-1'
               $scope.firstname = ''
               $scope.email = username
               $scope.image = ''

               if (email != null && password != null) {

                   //make http request
                   $http({
                       url: 'https://research-thushan.c9users.io:8080/beforecart/frs/signin/',
                       method: "GET",
                       params: { email: email, password: password }
                   }).then(function (response) {

                       var values = response.data
                       console.log('fetching-response data to $scope varaibles')
                       //fetch response to local variables
                       for (var item in values) {
                           console.log(values[item])
                           $scope.userId = values[item].userId
                           $scope.firstname = values[item].firstname
                           $scope.email = values[item].email
                           $scope.image = values[item].image
                           break
                       }
                       console.log('userId' + $scope.userId)
                       console.log('fistname' + $scope.firstname)
                       console.log('email ' + $scope.email)
                       console.log('image' + $scope.image)
                       if ($scope.userId != '-1') {
                           $scope.emailTxt = null
                           $scope.passwordTxt = null

                           try {
                               var prevUserId = window.localStorage.getItem('userId')// $cookieStore.get('userId')
                               var prevEmail = window.localStorage.getItem('email')// $cookieStore.get('email')
                           }
                           catch (err) {
                               console.log(err)
                               console.log('error in retrieving userId and email from $cookieStore at signIn()')
                               //$cookieStore.put('userId', null);
                               window.localStorage.setItem('userId', null)
                               window.localStorage.setItem('password', null)
                               //$cookieStore.put('email', '');
                               window.localStorage.setItem('email', '')
                               var prevUserId = window.localStorage.getItem('userId')//$cookieStore.get('userId');
                               var prevEmail = window.localStorage.getItem('email')  //$cookieStore.get('email');

                           }
                           if ((prevUserId == $scope.userId) || (prevUserId == null)) {  //same user again trying to signin or new user

                               //remove existing user attributes
                               //$cookieStore.remove('userId');
                               // $cookieStore.remove('email');
                               // $cookieStore.remove('firstname');
                               window.localStorage.removeItem('email')
                               window.localStorage.removeItem('password')
                               window.localStorage.removeItem('userId')
                               window.localStorage.removeItem('firstname')
                               //asssign new values
                               // $cookieStore.put('userId', $scope.userId)
                               //$cookieStore.put('firstname',$scope.firstname)
                               // $cookieStore.put('email', $scope.email)
                               window.localStorage.setItem('userId', $scope.userId)
                               window.localStorage.setItem('firstname', $scope.firstname)
                               window.localStorage.setItem('email', $scope.email)
                               window.localStorage.setItem('password', password)

                               if ($scope.image == '') { //check profile picture
                                   $scope.image = 'images/avatar.png'
                               }
                               window.localStorage.setItem('image', $scope.image)
                               console.log('email:' + $scope.email + " firstname:" + $scope.firstname + " userId:" + $scope.userId)
                               window.location = '#/app/mainCatelog'
                           }
                           else if ($scope.userId != prevUserId) {      //signin as new user
                               var alertPopup = $ionicPopup.alert({
                                   title: 'Finish Tour',
                                   template: 'You need to conclude previous shopping tour of ' + prevEmail + " to sign in as new user"
                               });
                           }



                       }
                       else {
                           $scope.emailTxt = email
                           $scope.passwordTxt = password

                           // An  error alert dialog
                           var alertPopup = $ionicPopup.alert({
                               title: 'Invalid credentials',
                               template: 'Please make sure to enter valid email address and password'
                           });
                       }

                   });

               }
               else {
                   // An  error alert dialog
                   var alertPopup = $ionicPopup.alert({
                       title: 'Invalid credentials',
                       template: 'Your email address and password are required to sign in'
                   });

               }



           }

           /**
            * refresh
            * @export
            */
           $scope.refresh = function () {
               //refresh binding
               $scope.$broadcast("scroll.refreshComplete");
           };
       }])

       /**
       *Chart controller
       *
       * @param {!angular.Scope} $rootScope,$scope, $http, $ionicLoading, $cookieStore
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("ChartController", function ($rootScope, $scope, $http, $ionicLoading, $cookieStore) {

            /**
            * state changed event
            * @param {Event} event
            * @param {State} toState
            * @param {State} fromState
            * @param {Params} fromParams
            * @export
            */
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toState.name == 'app.popularity') {
                    console.log('TO:app.popularity=CHART')
                    $scope.loadChart()
                }
            })

            /**
           *Load chart
           * @export
           */
            $scope.loadChart = function () {
                try {

                    var productId = window.localStorage.getItem('productId')  // $cookieStore.get('productId')//$scope.pid
                    console.log('loading Chart for pid' + productId)
                    $http({
                        url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/chart',
                        method: 'GET',
                        params: { pid: productId }

                    }).then(function (response) {
                        try {
                            var result = ''
                            result = response.data
                            var res = []
                            var str = result.toString()
                            var res = str.split(",")
                            var nav = res[0].split("/")
                            var sav = res[1].split("/")
                            var eur = res[2].split("/")
                            var asi = res[3].split("/")
                            var aus = res[4].split("/")
                            var afr = res[5].split("/")
                            var trend = res[6]

                            console.log(trend)

                            $scope.labels = ["North America", "South America", "Europe Union", "Australia", "Asia", "Africa"];
                            $scope.series = ['Positive Score', 'Negative Score'];
                            $scope.data = [
                             [nav[0], sav[0], eur[0], aus[0], asi[0], afr[0]],
                             [nav[1], sav[1], eur[1], aus[1], asi[1], afr[1]]

                            ];
                            $scope.NAP = "Positive Score : " + parseInt(nav[0], 10) + "%"
                            $scope.NAN = "Negative Score : " + parseInt(nav[1], 10) + "%"

                            $scope.SAP = "Positive Score : " + parseInt(sav[0], 10) + "%"
                            $scope.SAN = "Negative Score : " + parseInt(sav[1], 10) + "%"

                            $scope.EUP = "Positive Score : " + parseInt(eur[0], 10) + "%"
                            $scope.EUN = "Negative Score : " + parseInt(eur[1], 10) + "%"

                            $scope.AUP = "Positive Score : " + parseInt(aus[0], 10) + "%"
                            $scope.AUN = "Negative Score : " + parseInt(aus[1], 10) + "%"

                            $scope.ASP = "Positive Score : " + parseInt(asi[0], 10) + "%"
                            $scope.ASN = "Negative Score : " + parseInt(asi[1], 10) + "%"

                            $scope.AFP = "Positive Score : " + parseInt(afr[0], 10) + "%"
                            $scope.AFN = "Negative Score : " + parseInt(afr[1], 10) + "%"

                            $scope.TREND = "This product is " + parseInt(trend, 10) + "% Globally Trended!"

                            console.log('' + $scope.data)
                        }
                        catch (err) {
                            console.log('CHART_VIEW ERROR' + err.message)
                        }

                    });


                } catch (err) { console.log('CHAT_VIEW ERROR' + err.message) }
            }
        })

       /**
       *Map controller
       *
       * @param {!angular.Scope} $rootScope,$scope, $ionicLoading, $http, $cookieStore
       * @constructor
       * @ngInject
       * @export
       **/
        .controller('MapController', function ($rootScope, $scope, $ionicLoading, $http, $cookieStore) {

            /**
            * state changed event
            * @param {Event} event
            * @param {State} toState
            * @param {State} fromState
            * @param {Params} fromParams
            * @export
            */
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toState.name == 'app.popularity') {
                    console.log('TO:app.popularity=MAP')
                    $scope.loadMap()
                }
            })

            /**
           *Load map
           * @export
           */
            $scope.loadMap = function () {
                try {
                    $scope.initialise = function () {
                        var res = ""
                        console.log("In Google.maps.event.addDomListener");

                        var productId = window.localStorage.getItem('productId')
                        console.log('loading map for pid' + productId)
                        $http({
                            url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/map',
                            method: 'GET',
                            params: { pid: productId }

                        }).then(function (response) {

                            var result = response.data
                            var str = result.toString()
                            var res = str.split(",")


                            var AusLatlng = new google.maps.LatLng(-25.274398, 133.775136);
                            var AfrcLatlng = new google.maps.LatLng(-8.783195, 34.508523);
                            var USALatlng = new google.maps.LatLng(37.71859033, -95.712891);
                            var CanadaLatlng = new google.maps.LatLng(56.130366, -106.346771);
                            var ArgenLatlng = new google.maps.LatLng(-38.416097, -63.616672);
                            var SRLLatlng = new google.maps.LatLng(7.873054, 80.771797);
                            var INDIALatlng = new google.maps.LatLng(20.593684, 78.96288);
                            var ChnLatlng = new google.maps.LatLng(35.86166, 104.195397);
                            var JpnLatlng = new google.maps.LatLng(36.204824, 138.252924);
                            var KorLatlng = new google.maps.LatLng(35.907757, 127.766922);
                            var ThaiLatlng = new google.maps.LatLng(15.870032, 100.992541);
                            var SgnpLatlng = new google.maps.LatLng(1.352083, 103.819836);
                            var SpainLatlng = new google.maps.LatLng(40.463667, -3.74922);
                            var UKLatlng = new google.maps.LatLng(52.3555177, -1.1743197);
                            var TurkeyLatlng = new google.maps.LatLng(38.963745, 35.243322);
                            var FraLatlng = new google.maps.LatLng(46.227638, 2.213749);
                            var GerLatlng = new google.maps.LatLng(51.165691, 10.451526);
                            var ItalyLatlng = new google.maps.LatLng(41.87194, 12.56738);

                            var mapOptions = {
                                center: INDIALatlng,
                                zoom: 2,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            };



                            console.log(mapOptions);
                            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

                            var image = 'http://sj.uploads.im/t/oxy4I.png'



                            if (parseInt(res[0], 10) > 0) {
                                var infowindow0 = new google.maps.InfoWindow({
                                    content: parseInt(res[0], 10) + "% people from Australia talk about this"
                                });

                                var marker0 = new google.maps.Marker({
                                    position: AusLatlng,
                                    map: map,
                                    title: 'Popularity in Australia',
                                    icon: image
                                });

                                google.maps.event.addListener(marker0, 'click', function () {
                                    infowindow0.open(map, marker0);
                                });
                            }

                            if (parseInt(res[1], 10) > 0) {
                                var infowindow1 = new google.maps.InfoWindow({
                                    content: parseInt(res[1], 10) + "% people from Africa talk about this"
                                });

                                var marker1 = new google.maps.Marker({
                                    position: AfrcLatlng,
                                    map: map,
                                    title: 'Popularity in Africa',
                                    icon: image
                                });

                                google.maps.event.addListener(marker1, 'click', function () {
                                    infowindow1.open(map, marker1);
                                });
                            }

                            if (parseInt(res[2], 10) > 0) {
                                var infowindow2 = new google.maps.InfoWindow({
                                    content: parseInt(res[2], 10) + "% people from United States talk about this"
                                });

                                var marker2 = new google.maps.Marker({
                                    position: USALatlng,
                                    map: map,
                                    title: 'Popularity in United States',
                                    icon: image

                                });

                                google.maps.event.addListener(marker2, 'click', function () {
                                    infowindow2.open(map, marker2);
                                });
                            }

                            if (parseInt(res[3], 10) > 0) {
                                var infowindow3 = new google.maps.InfoWindow({
                                    content: parseInt(res[3], 10) + "% people from Canada talk about this"
                                });

                                var marker3 = new google.maps.Marker({
                                    position: CanadaLatlng,
                                    map: map,
                                    title: 'Popularity in Canada',
                                    icon: image
                                });

                                google.maps.event.addListener(marker3, 'click', function () {
                                    infowindow3.open(map, marker3);
                                });
                            }

                            if (parseInt(res[4], 10) > 0) {
                                var infowindow4 = new google.maps.InfoWindow({
                                    content: parseInt(res[4], 10) + "% people from Argentina talk about this"
                                });

                                var marker4 = new google.maps.Marker({
                                    position: ArgenLatlng,
                                    map: map,
                                    title: 'Popularity in Argentina',
                                    icon: image
                                });

                                google.maps.event.addListener(marker4, 'click', function () {
                                    infowindow4.open(map, marker4);
                                });
                            }

                            if (parseInt(res[5], 10) > 0) {
                                var infowindow5 = new google.maps.InfoWindow({
                                    content: parseInt(res[5], 10) + "% people from Sri Lanka talk about this"
                                });

                                var marker5 = new google.maps.Marker({
                                    position: SRLLatlng,
                                    map: map,
                                    title: 'Popularity in Sri Lanka',
                                    icon: image
                                });

                                google.maps.event.addListener(marker5, 'click', function () {
                                    infowindow5.open(map, marker5);
                                });
                            }

                            if (parseInt(res[6], 10) > 0) {
                                var infowindow6 = new google.maps.InfoWindow({
                                    content: parseInt(res[6], 10) + "% people from India talk about this"
                                });

                                var marker6 = new google.maps.Marker({
                                    position: INDIALatlng,
                                    map: map,
                                    title: 'Popularity in India',
                                    icon: image
                                });

                                google.maps.event.addListener(marker6, 'click', function () {
                                    infowindow6.open(map, marker6);
                                });
                            }

                            if (parseInt(res[7], 10) > 0) {
                                var infowindow7 = new google.maps.InfoWindow({
                                    content: parseInt(res[7], 10) + "% people from China talk about this"
                                });

                                var marker7 = new google.maps.Marker({
                                    position: ChnLatlng,
                                    map: map,
                                    title: 'Popularity in China',
                                    icon: image
                                });

                                google.maps.event.addListener(marker7, 'click', function () {
                                    infowindow7.open(map, marker7);
                                });
                            }

                            if (parseInt(res[8], 10) > 0) {
                                var infowindow8 = new google.maps.InfoWindow({
                                    content: parseInt(res[8], 10) + "% people from Japan talk about this"
                                });

                                var marker8 = new google.maps.Marker({
                                    position: JpnLatlng,
                                    map: map,
                                    title: 'Popularity in Japan',
                                    icon: image
                                });

                                google.maps.event.addListener(marker8, 'click', function () {
                                    infowindow8.open(map, marker8);
                                });
                            }


                            if (parseInt(res[9], 10) > 0) {
                                var infowindow9 = new google.maps.InfoWindow({
                                    content: parseInt(res[9], 10) + "% people from Korea talk about this"
                                });

                                var marker9 = new google.maps.Marker({
                                    position: KorLatlng,
                                    map: map,
                                    title: 'Popularity in Korea',
                                    icon: image
                                });

                                google.maps.event.addListener(marker9, 'click', function () {
                                    infowindow9.open(map, marker9);
                                });
                            }

                            if (parseInt(res[10], 10) > 0) {
                                var infowindow10 = new google.maps.InfoWindow({
                                    content: parseInt(res[10], 10) + "% people from Thailand talk about this"
                                });

                                var marker10 = new google.maps.Marker({
                                    position: ThaiLatlng,
                                    map: map,
                                    title: 'Popularity in Thailand',
                                    icon: image
                                });

                                google.maps.event.addListener(marker10, 'click', function () {
                                    infowindow10.open(map, marker10);
                                });
                            }

                            if (parseInt(res[11], 10) > 0) {
                                var infowindow11 = new google.maps.InfoWindow({
                                    content: parseInt(res[11], 10) + "% people from Singapore talk about this"
                                });

                                var marker11 = new google.maps.Marker({
                                    position: SgnpLatlng,
                                    map: map,
                                    title: 'Popularity in Singapore',
                                    icon: image
                                });

                                google.maps.event.addListener(marker11, 'click', function () {
                                    infowindow11.open(map, marker11);
                                });
                            }

                            if (parseInt(res[12], 10) > 0) {
                                var infowindow12 = new google.maps.InfoWindow({
                                    content: parseInt(res[12], 10) + "% people from Spain talk about this"
                                });

                                var marker12 = new google.maps.Marker({
                                    position: SpainLatlng,
                                    map: map,
                                    title: 'Popularity in Spain',
                                    icon: image
                                });

                                google.maps.event.addListener(marker12, 'click', function () {
                                    infowindow12.open(map, marker12);
                                });
                            }

                            if (parseInt(res[13], 10) > 0) {
                                var infowindow13 = new google.maps.InfoWindow({
                                    content: parseInt(res[13], 10) + "% people from United Kindom talk about this"
                                });

                                var marker13 = new google.maps.Marker({
                                    position: UKLatlng,
                                    map: map,
                                    title: 'Popularity in United Kindom',
                                    icon: image
                                });

                                google.maps.event.addListener(marker13, 'click', function () {
                                    infowindow13.open(map, marker13);
                                });
                            }

                            if (parseInt(res[14], 10) > 0) {
                                var infowindow14 = new google.maps.InfoWindow({
                                    content: parseInt(res[14], 10) + "% people from Turkey talk about this"
                                });

                                var marker14 = new google.maps.Marker({
                                    position: TurkeyLatlng,
                                    map: map,
                                    title: 'Popularity in Turkey',
                                    icon: image
                                });

                                google.maps.event.addListener(marker14, 'click', function () {
                                    infowindow14.open(map, marker14);
                                });
                            }

                            if (parseInt(res[15], 10) > 0) {
                                var infowindow15 = new google.maps.InfoWindow({
                                    content: parseInt(res[15], 10) + "% people from France talk about this"
                                });

                                var marker15 = new google.maps.Marker({
                                    position: FraLatlng,
                                    map: map,
                                    title: 'Popularity in France',
                                    icon: image
                                });

                                google.maps.event.addListener(marker15, 'click', function () {
                                    infowindow15.open(map, marker15);
                                });
                            }

                            if (parseInt(res[16], 10) > 0) {
                                var infowindow16 = new google.maps.InfoWindow({
                                    content: parseInt(res[16], 10) + "% people from Germany talk about this"
                                });

                                var marker16 = new google.maps.Marker({
                                    position: GerLatlng,
                                    map: map,
                                    title: 'Popularity in Germany',
                                    icon: image
                                });

                                google.maps.event.addListener(marker16, 'click', function () {
                                    infowindow16.open(map, marker16);
                                });
                            }

                            if (parseInt(res[17], 10) > 0) {
                                var infowindow17 = new google.maps.InfoWindow({
                                    content: parseInt(res[17], 10) + "% people from Italy talk about this"
                                });

                                var marker17 = new google.maps.Marker({
                                    position: ItalyLatlng,
                                    map: map,
                                    title: 'Popularity in Italy',
                                    icon: image
                                });

                                google.maps.event.addListener(marker17, 'click', function () {
                                    infowindow17.open(map, marker17);
                                });
                            }
                            $scope.map = map;

                        });

                    };
                    google.maps.event.addDomListener(document.getElementById("map"), 'load', $scope.initialise());
                }
                catch (err) { alert('Something is wrong. Unable to show the map') }
            }

        })

       /**
       *More View controller
       *
       * @param {!angular.Scope} $rootScope,$scope, $ionicLoading, $http, $cookieStore
       * @constructor
       * @ngInject
       * @export
       **/
        .controller("MoreViewController", function ($rootScope, $scope, $ionicLoading, $http, $cookieStore) {


            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                if (toState.name == 'app.popularity') {
                    console.log('TO:app.popularity=MORE-DETAILS')
                    $scope.LoadMoreDetails()
                }
            })


            /**
            *Load more details
            * @export
            */
            $scope.LoadMoreDetails = function () {

                try {
                    var productId = window.localStorage.getItem('productId')// $cookieStore.get('productId')

                    $http({
                        url: 'https://research-thushan.c9users.io:8080/beforecart/gmi/more',
                        method: 'GET',
                        params: { pid: productId }

                    }).then(function (response) {

                        var result = response.data
                        //alert(result)
                        var str = result.toString()
                        console.log(str);
                        var res = str.split(",")

                        $scope.CATEGORY = res[0]
                        $scope.PRICE = res[1]
                        $scope.STYLE = res[2]
                        $scope.COLOR = res[3]
                        $scope.SIZE = res[4]
                        $scope.STALLNAME = res[6] + "\n" + res[7] + "\n" + res[8] + "\n" + res[9]
                        $scope.DES = res[11]
                        $scope.link = res[10]
                        $scope.pImage = res[5]
                        console.log('' + $scope.data)

                    });

                }
                catch (err) {
                    //alert(err)
                    console.log(err)
                }
            }
        })

       /**
       *Error controller
       *
       * @param {!angular.Scope} $scope, $timeout, $ionicScrollDelegate, $cookieStore
       * @constructor
       * @ngInject
       * @export
       **/
    .controller("errorCtrl", ["$scope", "$rootScope", "$ionicLoading", "myappService", function ($scope, $rootScope, $ionicLoading, myappService) {
        //errorCtrl managed the display of error messages bubbled up from other controllers, directives, myappService
        //public properties that define the error message and if an error is present
        $scope.error = "";
        $scope.activeError = false;

        $rootScope.$on('loading:show', function () {
            $ionicLoading.show({ template: '<img src="images/comment.gif" style="width:40px;height:40px;" ></img><p>Please wait...</p>', noBackdrop: false })
        })

        $rootScope.$on('loading:hide', function () {
            $ionicLoading.hide()
        })





        //$ionicLoading.hide()



        //function to dismiss an active error
        $scope.dismissError = function () {
            $scope.activeError = false;
        };

        //broadcast event to catch an error and display it in the error section
        $scope.$on("error", function (evt, val) {
            //set the error message and mark activeError to true
            $scope.error = val;
            $scope.activeError = true;

            //stop any waiting indicators (including scroll refreshes)
            myappService.wait(false);
            $scope.$broadcast("scroll.refreshComplete");

            //manually apply given the way this might bubble up async
            $scope.$apply();
        });
    }]);
})();