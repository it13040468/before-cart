﻿(function () {
    "use strict";

    angular.module("myapp.services", []).factory("myappService", ["$rootScope", "$http", function ($rootScope, $http) {
        var myappService = {};

        //starts and stops the application waiting indicator
        myappService.wait = function (show) {
            if (show)
                $(".spinner").show();
            else
                $(".spinner").hide();
        };

        //load the friends of user
        myappService.sayHello = function (userId) {

            //alert('triggered')
            //console.log(' sayHello() service triggerd')

        }


        return myappService;
    }]);

    
})();